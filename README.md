Installaion:
1. Open the project in IntelliJ IDEA.
2. Use java version "1.8.0_352" and set up Tomcat8.5.84 server.
3. Install all the necessary dependencies.
4. Set up the MySQL database by running the provided SQL script and connect to the localhost.
5. Start the server.
6. Navigate to http://localhost:8080 in your web browser to access the website.

Technology used:
1. Language: Java, JavaScript, HTML, CSS
2. Framework: Spring Boot
3. Servlet: JSP
4. Server: Tomcat8.5.84
5. Database: MySQL

Contact:
If you have any question or you find problems in the project, please contact me by email: urnotszwjoy@126.com.
