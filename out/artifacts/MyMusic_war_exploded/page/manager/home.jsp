<%@ page import="com.martinwj.mymusic.util.ServletUtils" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%--
  Created by IntelliJ IDEA.
  Creater: Shao Shizhuo
  Date: 2023/1/11
  Time: 12:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
    <title>Background Management</title>
    <link href="${pageContext.request.contextPath }/css/manager/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath }/css/manager/fonts.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath }/css/manager/style.css" rel="stylesheet">
</head>

<body>

<%
    ServletUtils.setNumber(request.getSession());
%>

<!--Main-->
<main class="ftdms-layout-content">

    <div class="container-fluid" style="margin-bottom:90px;">

        <div class="row" style="margin-top:15px;">
            <div class="col-sm-6 col-lg-3">
                <div class="card bg-primary">
                    <div class="card-body clearfix">
                        <div class="pull-left">
                            <p class="h6 text-white m-t-0">The Number of Singers</p>
                            <p class="h3 text-white m-b-0"><fmt:formatNumber type="number" maxFractionDigits="3" value="${sessionScope.singerNumber}" /></p>
                        </div>
                        <div class="pull-right"> <span class="img-avatar img-avatar-48 bg-translucent"><i class="ftsucai-56 fa-1-5x"></i></span> </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-lg-3">
                <div class="card bg-danger">
                    <div class="card-body clearfix">
                        <div class="pull-left">
                            <p class="h6 text-white m-t-0">The Number of Songs</p>
                            <p class="h3 text-white m-b-0"><fmt:formatNumber type="number" maxFractionDigits="3" value="${sessionScope.songNumber}" /></p>
                        </div>
                        <div class="pull-right"> <span class="img-avatar img-avatar-48 bg-translucent"><i class="ftsucai-310 fa-1-5x"></i></span> </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-lg-3">
                <div class="card bg-success">
                    <div class="card-body clearfix">
                        <div class="pull-left">
                            <p class="h6 text-white m-t-0">The Number of Comments</p>
                            <p class="h3 text-white m-b-0"><fmt:formatNumber type="number" maxFractionDigits="3" value="${sessionScope.commentNumber}" /></p>
                        </div>
                        <div class="pull-right"> <span class="img-avatar img-avatar-48 bg-translucent"><i class="ftsucai-55 fa-1-5x"></i></span> </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-lg-3">
                <div class="card bg-purple">
                    <div class="card-body clearfix">
                        <div class="pull-left">
                            <p class="h6 text-white m-t-0">The Number of Users</p>
                            <p class="h3 text-white m-b-0"><fmt:formatNumber type="number" maxFractionDigits="3" value="${sessionScope.userNumber}" /></p>
                        </div>
                        <div class="pull-right"> <span class="img-avatar img-avatar-48 bg-translucent"><i class="ftsucai-310 fa-1-5x"></i></span> </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

</main>
<!--End Main page content-->

<script type="text/javascript" src="${pageContext.request.contextPath }/js/manager/jquery.min.js"></script>

</body>
</html>