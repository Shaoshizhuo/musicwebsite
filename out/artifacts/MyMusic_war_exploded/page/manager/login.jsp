<%--
  Created by IntelliJ IDEA.
  Creater: Shao Shizhuo
  Date: 2023/1/12
  Time: 18:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Admin Login</title>

    <link href="${pageContext.request.contextPath}/css/styles.css" rel="stylesheet" type="text/css" />
    <link href="${pageContext.request.contextPath}/css/demo.css" rel="stylesheet" type="text/css" />
    <link href="${pageContext.request.contextPath}/css/loaders.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.5.1.min.js"></script>
</head>
<body>
<div class='login'>
    <div class='login_title'>
        <span>Admin Login</span>
    </div>
    <div class='login_fields'>
        <div class='login_fields__user'>
            <div class='icon'>
                <img alt="" src='../../img/user_icon_copy.png' />
            </div>
            <input name="login" placeholder='Name' maxlength="16" type='text' autocomplete="off" />
            <div class='validation'>
                <img alt="" src='../../img/tick.png' />
            </div>
        </div>
        <div class='login_fields__password'>
            <div class='icon'>
                <img alt="" src='../../img/lock_icon_copy.png' />
            </div>
            <input name="pwd" placeholder='Password' maxlength="16" type='text' autocomplete="off" />
            <div class='validation'>
                <img alt="" src='../../img/tick.png' />
            </div>
        </div>
        <div class='login_fields__password'>
            <div class='icon'>
                <img alt="" src='../../img/key.png' />
            </div>
            <input name="code" placeholder='Verification' maxlength="4" type='text' name="ValidateNum" autocomplete="off" />
            <div class='validation' style="margin-left: 200px;opacity: 1; right: -120px;top: -3px;">
                <canvas class="J_codeimg" id="myCanvas" onclick="Code();">Sorry, please try another browser!</canvas>
            </div>
        </div>
        <div class='login_fields__submit'>
            <input type='button' value='Login' />
        </div>
    </div>
    <div class='success'>
    </div>
</div>
<div class='authent'>
    <div class="loader" style="height: 44px;width: 44px;margin-left: 28px;">
        <div class="loader-inner ball-clip-rotate-multiple">
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>
    <p>Loading</p>
</div>
<div class="OverWindows"></div>

<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-ui.min.js"></script>
<script type="text/javascript" src='${pageContext.request.contextPath}/js/stopExecutionOnTimeout.js'></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/js/Particleground.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/Treatment.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.mockjax.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jstool/tool.js"></script>
<script type="text/javascript">
    var canGetCookie = 0;//Whether cookies are stored. 0 No/1 Yes
    var ajaxmockjax = 0;//Whether to enable virtual Ajax request ring 0 Disable/1 Enable
    //Default account&password

    var truelogin = "123456";
    var truepwd = "123456";

    // Verification Code url
    var checkCodeUrl = getPath() + "/checkCodeServlet";

    // Login url
    var loginUrl = getPath() + "/admin/loginServlet";

    var CodeVal = 0;

    function Code() {
        // if (canGetCookie == 1) {
        //     createCode("AdminCode");
        //     var AdminCode = getCookieValue("AdminCode");
        //     showCheck(AdminCode);
        // } else {
        //     showCheck(createCode(""));
        // }
        var date = new Date();
        $.post(checkCodeUrl,
            {"date": date.getTime() },
            function (data){
                showCheck(data.code);
            },
            "json"
        );
    }

    Code();

    function showCheck(a) {
        CodeVal = a;
        var c = document.getElementById("myCanvas");
        var ctx = c.getContext("2d");
        ctx.clearRect(0, 0, 1000, 1000);
        ctx.font = "80px 'Hiragino Sans GB'";
        ctx.fillStyle = "#E8DFE8";
        ctx.fillText(a, 0, 100);
    }
    $(document).keypress(function (e) {
        // Return Button
        if (e.which == 13) {
            $('input[type="button"]').click();
        }
    });
    //Non-null verification
    $('input[type="button"]').click(function () {
        var login = $('input[name="login"]').val();
        var pwd = $('input[name="pwd"]').val();
        var code = $('input[name="code"]').val();
        if (login == '') {
            ErroAlert('Please input name again!');
        } else if (pwd == '') {
            ErroAlert('Please input password again!');
        } else if (code == '' || code.length != 4) {
            ErroAlert('Please input verification again!');
        } else {
            //Loading..
            // fullscreen();
            $('.login').addClass('test'); //Tilt effect
            setTimeout(function () {
                $('.login').addClass('testtwo'); //Translation effect
            }, 300);
            setTimeout(function () {
                $('.authent').show().animate({ right: -320 }, {
                    easing: 'easeOutQuint',
                    duration: 600,
                    queue: false
                });
                $('.authent').animate({ opacity: 1 }, {
                    duration: 200,
                    queue: false
                }).addClass('visible');
            }, 500);

            //Login
            var JsonData = { "login": login, "pwd": pwd, "code": code };
            var This = $(this);

            AjaxPost(
                loginUrl,
                JsonData,
                function () {
                    //ajax load
                    This.html('Loading...');
                },
                function (data) {
                    //ajax return
                    //Finish Verifying
                    setTimeout(function () {
                        $('.authent').show().animate({ right: 90 }, {
                            easing: 'easeOutQuint',
                            duration: 600,
                            queue: false
                        });
                        $('.authent').animate({ opacity: 0 }, {
                            duration: 200,
                            queue: false
                        }).addClass('visible');
                        $('.login').removeClass('testtwo'); //Tilt effect
                    }, 2000);
                    setTimeout(function () {
                        $('.authent').hide();
                        $('.login').removeClass('test');
                        if (data.Status == 'ok') {
                            //Login Successfully
                            $('.login div').fadeOut(100);
                            $('.success').fadeIn(1000);
                            $('.success').html(data.Text);
                            //Jump operation
                            $(location).attr('href', getPath() + "/page/manager/index.jsp");
                        } else {
                            AjaxErro(data);
                        }
                    }, 2400);
                })

        }
    })
    var fullscreen = function () {
        elem = document.body;
        if (elem.webkitRequestFullScreen) {
            elem.webkitRequestFullScreen();
        } else if (elem.mozRequestFullScreen) {
            elem.mozRequestFullScreen();
        } else if (elem.requestFullScreen) {
            elem.requestFullscreen();
        } else {
        }
    }

</script>

</body>
</html>
