<%--
  Created by IntelliJ IDEA.
  Creater: Shao Shizhuo
  Date: 2023/1/10
  Time: 22:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
    <title>Add new singer</title>
    <link rel="icon" href="favicon.ico" type="image/ico">
    <link href="${pageContext.request.contextPath }/css/manager/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath }css/manager/fonts.css" rel="stylesheet">
    <!--Tag insertion-->
    <link rel="stylesheet" href="${pageContext.request.contextPath }/js/manager/jquery-tags-input/jquery.tagsinput.min.css">
    <link href="${pageContext.request.contextPath }/css/manager/style.css" rel="stylesheet">
    <!--Time selector insertion-->
    <link rel="stylesheet" href="${pageContext.request.contextPath }/js/manager/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css">
    <!--Date selector insertion-->
    <link rel="stylesheet" href="${pageContext.request.contextPath }/js/manager/bootstrap-datepicker/bootstrap-datepicker3.min.css">
</head>

<body>
<!--Main-->
<main class="ftdms-layout-content">

    <div class="container-fluid" style="margin-bottom:90px;">

        <div class="row" style="margin-top:15px;">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">

                        <form action="${pageContext.request.contextPath}/admin/addSingerServlet" method="post" class="row" enctype="multipart/form-data" onsubmit="return checkInput()">

                            <div class="form-group col-md-12">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" id="name" name="name" value="" />
                            </div>
                            <div class="form-group col-md-12">
                                <label for="englishName">English Name</label>
                                <input type="text" class="form-control" id="englishName" name="englishName" value="" />
                            </div>
                            <div class="form-group col-md-12">
                                <label for="sex">Gender</label>
                                <div class="form-controls">
                                    <select name="sex" class="form-control" id="sex" value="">
                                        <option value="0">Female</option>
                                        <option value="1">Male</option>
                                        <option value="2">Group</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="birth">Birth</label>
                                <input class="form-control js-datetimepicker" type="text" id="birth" name="birth" value="" data-side-by-side="true" data-locale="zh-cn" data-format="YYYY-MM-DD" />
                            </div>
                            <div class="form-group col-md-12">
                                <label for="location">Country</label>
                                <input type="text" class="form-control" id="location" name="location" value="" />
                            </div>
                            <div class="form-group col-md-12">
                                <label for="introduction">Introduction</label>
                                <textarea class="form-control" id="introduction" name="introduction" rows="5" value=""></textarea>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="musicImg" class="control-label">Image：</label>
                                <input type="file" id="musicImg" name="musicImg">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="debutDate">Debut Date</label>
                                <input class="form-control js-datetimepicker" type="text" id="debutDate" name="debutDate" value="" data-side-by-side="true" data-locale="zh-cn" data-format="YYYY-MM-DD" />
                            </div>
                            <div class="form-group col-md-12">
                                <label for="accessCount">Initial heat</label>
                                <input type="text" class="form-control" id="accessCount" name="accessCount" value="" />
                            </div>
                            <div class="form-group col-md-12">
                                <button type="submit" class="btn btn-primary ajax-post" target-form="add-form">Confirm</button>
                                <button type="button" class="btn btn-default" onclick="javascript:history.back(-1);return false;">Return</button>
                                <a href="${pageContext.request.contextPath}/page/user/index.jsp">&nbsp; &nbsp;Return to the main page</a>
                            </div>
                        </form>

                    </div>
                </div>
            </div>

        </div>

    </div>

</main>
<!--End Main page content-->
</div>
</div>

<script type="text/javascript" src="${pageContext.request.contextPath }/js/manager/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath }/js/manager/bootstrap.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath }/js/manager/perfect-scrollbar.min.js"></script>
<!-- Tag insertion -->
<script type="text/javascript" src="${pageContext.request.contextPath }/js/manager/jquery-tags-input/jquery.tagsinput.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath }/js/manager/main.min.js"></script>
<!--Time selector insertion-->
<script src="${pageContext.request.contextPath }/js/manager/bootstrap-datetimepicker/moment.min.js"></script>
<script src="${pageContext.request.contextPath }/js/manager/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<%--<script src="${pageContext.request.contextPath }/js/manager/bootstrap-datetimepicker/locale/zh-cn.js"></script>--%>
<!--Date selector insertion-->
<script src="${pageContext.request.contextPath }/js/manager/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<%--<script src="${pageContext.request.contextPath }/js/manager/bootstrap-datepicker/locales/bootstrap-datepicker.zh-CN.min.js"></script>--%>
<script type="text/javascript" src="${pageContext.request.contextPath }/js/manager/main.min.js"></script>
<script type="text/javascript">
    !function(a){
        a.fn.datepicker.dates["zh-CN"]={
            days:["Sun","Mon","Tue","Wed","Thu","Fri","Sat"],
            daysShort:["Sun","Mon","Tue","Wed","Thu","Fri","Sat"],
            daysMin:["Sun","Mon","Tue","Wed","Thu","Fri","Sat"],
            months:["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],
            monthsShort:["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],
            today:"Today",clear:"Clear",format:"yyyy/mm/dd",titleFormat:"yyyy/mm",weekStart:1
        }
    }(jQuery);

    (function (global, factory) {
        typeof exports === 'object' && typeof module !== 'undefined' ? factory(require('../moment')) :
            typeof define === 'function' && define.amd ? define(['moment'], factory) :
                factory(global.moment)
    }(this, function (moment) { 'use strict';

        var zh_cn = moment.defineLocale('zh-cn', {
            months : 'Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec'.split('_'),
            monthsShort : 'Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec'.split('_'),
            weekdays : 'Sun_Mon_Tue_Wed_Thu_Fri_Sat'.split('_'),
            weekdaysShort : 'Sun_Mon_Tue_Wed_Thu_Fri_Sat'.split('_'),
            weekdaysMin : 'Sun_Mon_Tue_Wed_Thu_Fri_Sat'.split('_'),
            longDateFormat : {
                LT : 'Ah/mm',
                LTS : 'Ah/m/s',
                L : 'YYYY-MM-DD',
                LL : 'YYYY/MM/DD',
                LLL : 'YYYY/MM/DD/Ah/mm',
                LLLL : 'YYYY/MM/DD/Ah/mm',
                l : 'YYYY-MM-DD',
                ll : 'YYYY/MM/DD',
                lll : 'YYYY/MM/DD/Ah/mm',
                llll : 'YYYY/MM/DD/Ah/mm'
            },
            meridiemParse: /Early morning|Morning|Noon|Afternoon|Evening|Night/,
            meridiemHour: function (hour, meridiem) {
                if (hour === 12) {
                    hour = 0;
                }
                if (meridiem === 'Early morning' || meridiem === 'Morning' ||
                    meridiem === 'Noon') {
                    return hour;
                } else if (meridiem === 'Evening' || meridiem === 'Night') {
                    return hour + 12;
                } else {
                    return hour >= 11 ? hour : hour + 12;
                }
            },
            meridiem : function (hour, minute, isLower) {
                var hm = hour * 100 + minute;
                if (hm < 600) {
                    return 'Early morning';
                } else if (hm < 900) {
                    return 'Morning';
                } else if (hm < 1130) {
                    return 'Noon';
                } else if (hm < 1230) {
                    return 'Afternoon';
                } else if (hm < 1800) {
                    return 'Evening';
                } else {
                    return 'Night';
                }
            },
            calendar : {
                sameDay : function () {
                    return this.minutes() === 0 ? '[Today]Ah[ ]' : '[Today]LT';
                },
                nextDay : function () {
                    return this.minutes() === 0 ? '[Tomorrow]Ah[ ]' : '[Tomorrow]LT';
                },
                lastDay : function () {
                    return this.minutes() === 0 ? '[Yesterday]Ah[ ]' : '[Yesterday]LT';
                },
                nextWeek : function () {
                    var startOfWeek, prefix;
                    startOfWeek = moment().startOf('week');
                    prefix = this.unix() - startOfWeek.unix() >= 7 * 24 * 3600 ? '[ ]' : '[ ]';
                    return this.minutes() === 0 ? prefix + 'dddAh' : prefix + 'ddd/Ah/mm';
                },
                lastWeek : function () {
                    var startOfWeek, prefix;
                    startOfWeek = moment().startOf('week');
                    prefix = this.unix() < startOfWeek.unix()  ? '[ ]' : '[ ]';
                    return this.minutes() === 0 ? prefix + 'dddAh' : prefix + 'ddd/Ah/mm';
                },
                sameElse : 'LL'
            },
            ordinalParse: /\d{1,2}(Day|Month|Week)/,
            ordinal : function (number, period) {
                switch (period) {
                    case 'd':
                    case 'D':
                    case 'DDD':
                        return number + 'Day';
                    case 'M':
                        return number + 'Month';
                    case 'w':
                    case 'W':
                        return number + 'Week';
                    default:
                        return number;
                }
            },
            relativeTime : {
                future : '%sIn',
                past : '%sBefore',
                s : 'Second',
                m : '1 minute',
                mm : '%d minutes',
                h : '1 hour',
                hh : '%d hours',
                d : '1 day',
                dd : '%d days',
                M : '1 month',
                MM : '%d months',
                y : '1 year',
                yy : '%d years'
            },
            week : {
                dow : 1, // Monday is the first day of the week.
                doy : 4  // The week that contains Jan 4th is the first week of the year.
            }
        });
        return zh_cn;
    }));
</script>

<script type="text/javascript">

    function checkInput() {
        tips.loading("show");
        var name = $("#name").val();
        var birth = $("#birth").val();
        var location = $("#location").val();
        var debutDate = $("#debutDate").val();
        var englishName = $("#englishName").val();
        var englishNameReg = /^[A-Z]+$/;
        var accessCountStr = $("#accessCount").val();
        var reg = /^\d+$/;
        if(!reg.test(accessCountStr)){
            tips.loading("hide");
            tips.notify('Should be Non-negative integer~', 'danger', 100);
            return false;
        }
        var accessCount = Number.parseInt(accessCountStr);

        if (name == null || name == "") {
            tips.loading("hide");
            tips.notify('Singer Name Cannot Null~', 'danger', 100);
            return false;
        }
        if (englishName == null || englishName == "") {
            tips.loading("hide");
            tips.notify('Singer English Name Cannot Null~', 'danger', 100);
            return false;
        }
        if (englishNameReg.test(englishName)) {
            tips.loading("hide");
            tips.notify('Singer English Name Must Be Uppercase~', 'danger', 100);
            return false;
        }
        if (birth == null || birth == "") {
            tips.loading("hide");
            tips.notify('Birth Cannot Null~', 'danger', 100);
            return false;
        }
        if (location == null || location == "") {
            tips.loading("hide");
            tips.notify('Country Cannot Null~', 'danger', 100);
            return false;
        }
        if (debutDate == null || debutDate == "") {
            tips.loading("hide");
            tips.notify('Debut Date Cannot Null~', 'danger', 100);
            return false;
        }
        if (accessCount < 0 || accessCount > 5000) {
            tips.loading("hide");
            tips.notify('Initial heat should between 0~5000~', 'danger', 100);
            return false;
        }
        return true;
    }

</script>
</body>
</html>
