<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%--
  Created by IntelliJ IDEA.
  Creater: Shao Shizhuo
  Date: 2023/1/11
  Time: 9:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
    <link rel="icon" href="favicon.ico" type="image/ico">
    <title>Comment Manage</title>
    <link href="${pageContext.request.contextPath }/css/manager/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath }/css/manager/fonts.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath }/css/manager/style.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath }/css/manager/animate.css" rel="stylesheet">
    <script type="text/javascript" src="${pageContext.request.contextPath }/js/jstool/tool.js"></script>
    <link rel="stylesheet" href="${pageContext.request.contextPath }/js/manager/jquery-tags-input/jquery.tagsinput.min.css">
</head>

<body>
<!--Main-->
<main class="ftdms-layout-content">

    <div class="container-fluid" style="margin-bottom:90px;">

        <div class="row" style="margin-top:15px;">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-toolbar clearfix">
                        <form class="pull-right search-bar" method="get" action="#" role="form">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <input type="text" hidden name="search_field" id="search-field" value="up"/>
                                    <button class="btn btn-default dropdown-toggle" id="search-btn" data-toggle="dropdown" type="button" aria-haspopup="true" aria-expanded="false">
                                        Select <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li> <a tabindex="-1" href="javascript:void(0)" data-field="up">Likes Number</a> </li>
                                        <li> <a tabindex="-1" href="javascript:void(0)" data-field="type">Type</a> </li>
                                        <li> <a tabindex="-1" href="javascript:void(0)" data-field="username">User Name</a> </li>
                                    </ul>
                                </div>
                                <input type="text" class="form-control" value="${sessionScope.commentKeyName}" id="keyName" name="keyName" placeholder=" ">
                            </div>
                        </form>
                        <div class="toolbar-btn-action">
                            <a class="btn submenuitem btn-success m-r-5 " href="${pageContext.request.contextPath}/admin/findCommentByPageServlet?currentPage=${sessionScope.commentPb.currentPage}&rows=6&type=${sessionScope.commentCondition.type[0]}&up=${sessionScope.commentCondition.up[0]}&username=${sessionScope.commentCondition.username[0]}" data-id="link553" data-index="553">Refresh</a>
                            <a class="btn submenuitem btn-danger" href="javascript:delSelected();" data-id="link555" data-index="555">Delete Selection</a>
                        </div>
                    </div>
                    <div class="card-body">

                        <div class="table-responsive">
<%--                            <form id="form_del" action="${pageContext.request.contextPath}/admin/deleteCommentServlet" method="post">--%>
                                <table class="table table-bordered" >
                                    <thead>
                                    <tr>
                                        <th width="5">
                                            <label class="ftdms-checkbox checkbox-primary">
                                                <input type="checkbox" id="check-all"><span></span>
                                            </label>
                                        </th>
                                        <th>No.</th>
                                        <th>User Name</th>
                                        <th>Type</th>
                                        <th>Singer-Song</th>
                                        <th>Content</th>
                                        <th>Date</th>
                                        <th>Likes Number</th>
                                        <th>Operation</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach items="${sessionScope.commentPb.list}" var="comment" varStatus="s">
                                        <tr>
                                            <td>
                                                <label class="ftdms-checkbox checkbox-primary">
                                                    <input type="checkbox" id="commentId" name="commentId" value="${comment.id}"/><span></span>
                                                </label>
                                            </td>
                                            <td>${s.count}</td>
                                            <td>${comment.username}</td>
                                            <td>${comment.type == 0 ? "songName" : (comment.type == 1 ? "songListName" : "singerName")}</td>
                                            <td>
                                                <c:if test="${comment.type == 0}">
                                                    ${comment.songName}
                                                </c:if>
                                                <c:if test="${comment.type == 1}">
                                                    ${comment.songListName}
                                                </c:if>
                                                <c:if test="${comment.type == 2}">
                                                    ${comment.singerName}
                                                </c:if>
                                            </td>
                                            <td>
                                                <button type="button" id="btn_${comment.id}" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg_${comment.id}">Content</button>
                                                <div class="modal fade bs-example-modal-lg_${comment.id}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
                                                    <div class="modal-dialog modal-lg" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                                                <h4 class="modal-title" id="myLargeModalLabel">
                                                                    <c:if test="${comment.type == 0}">
                                                                        ${comment.songName}
                                                                    </c:if>
                                                                    <c:if test="${comment.type == 1}">
                                                                        ${comment.songListName}
                                                                    </c:if>
                                                                    <c:if test="${comment.type == 2}">
                                                                        ${comment.singerName}
                                                                    </c:if>
                                                                </h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                ${comment.context}
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" id="modal_${comment.id}" data-dismiss="modal">Close</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>${comment.createDate}</td>
                                            <td>${comment.up}</td>
                                            <td>
                                                <div class="btn-group">
                                                    <a type="buttion" class="btn btn-primary btn-xs btn-default submenuitem" data-toggle="modal" data-target="#exampleModal_${comment.id}" data-whatever="@mdo" target= "_self" title="Edit"><i class="ftsucai-edit-2"></i></a>
                                                    <div class="modal fade" id="exampleModal_${comment.id}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                    <h4 class="modal-title" id="exampleModalLabel_${comment.id}">Edit User<${comment.username}> Context content：</h4>
                                                                </div>
                                                                <form id="form_update_${comment.id}" action="${pageContext.request.contextPath}/admin/updateCommentServlet?commentId=${comment.id}" method="post">
                                                                    <div class="modal-body">
                                                                        <div class="form-group">
                                                                            <label for="updateContext_${comment.id}" class="control-label">Comment content：</label>
                                                                            <textarea class="form-control" id="updateContext_${comment.id}" name="updateContext_${comment.id}" >${comment.context}</textarea>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                    <button type="button" class="btn btn-primary" onclick="updateComment(${comment.id})" >Confirm Edit</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <a class="btn btn-xs btn-default submenuitem" href="javascript:deleteComment(${comment.id})" target= "_self" title="Delete" data-toggle="tooltip"><i class="ftsucai-del-2"></i></a>
                                                </div>
                                            </td>

                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
<%--                            </form>--%>
                        </div>

                        <nav>
                            <ul class="pagination  no-border">
                                <c:if test="${sessionScope.commentPb.totalCount != 0}">

                                    <c:if test="${sessionScope.commentPb.currentPage == 1}">
                                        <li class="disabled">
                                            <a class="btn submenuitem" href="javascript:void(0);">
                                                <span><i class="ftsucai-146"></i></span>
                                            </a>
                                        </li>
                                    </c:if>
                                    <c:if test="${sessionScope.commentPb.currentPage != 1}">
                                        <li>
                                            <a class="btn submenuitem" href="${pageContext.request.contextPath}/admin/findCommentByPageServlet?currentPage=${sessionScope.commentPb.currentPage - 1}&rows=6&type=${sessionScope.commentCondition.type[0]}&up=${sessionScope.commentCondition.up[0]}&username=${sessionScope.commentCondition.username[0]}">
                                                <span><i class="ftsucai-146"></i></span>
                                            </a>
                                        </li>
                                    </c:if>


                                    <c:forEach begin="${sessionScope.commentPb.startPage}" end="${sessionScope.commentPb.endPage}" var="i" >
                                        <c:if test="${sessionScope.commentPb.currentPage == i}">
                                            <li class="active"><a  class="btn submenuitem" id="currentPage"
                                                                   href="${pageContext.request.contextPath}/admin/findCommentByPageServlet?currentPage=${i}&rows=6&type=${sessionScope.commentCondition.type[0]}&up=${sessionScope.commentCondition.up[0]}&username=${sessionScope.commentCondition.username[0]}">${i}</a></li>
                                        </c:if>
                                        <c:if test="${sessionScope.commentPb.currentPage != i}">
                                            <li><a  class="submenuitem" href="${pageContext.request.contextPath}/admin/findCommentByPageServlet?currentPage=${i}&rows=6&type=${sessionScope.commentCondition.type[0]}&up=${sessionScope.commentCondition.up[0]}&username=${sessionScope.commentCondition.username[0]}">${i}</a></li>
                                        </c:if>
                                    </c:forEach>

                                    <c:if test="${sessionScope.commentPb.currentPage == sessionScope.commentPb.totalPage}">
                                        <li class="disabled">
                                            <a class="btn submenuitem"
                                               href="javascript:void(0);">
                                                <span><i class="ftsucai-139"></i></span>
                                            </a>
                                        </li>
                                    </c:if>
                                    <c:if test="${sessionScope.commentPb.currentPage != sessionScope.commentPb.totalPage}">
                                        <li>
                                            <a class="btn submenuitem"
                                               href="${pageContext.request.contextPath}/admin/findCommentByPageServlet?currentPage=${sessionScope.commentPb.currentPage + 1}&rows=6&type=${sessionScope.commentCondition.type[0]}&up=${sessionScope.commentCondition.up[0]}&username=${sessionScope.commentCondition.username[0]}">
                                                <span><i class="ftsucai-139"></i></span>
                                            </a>
                                        </li>
                                    </c:if>

                                    <span style="font-size: 25px;margin-left: 5px;">
                                    Total ${sessionScope.commentPb.totalCount} Records/Total ${sessionScope.commentPb.totalPage} Pages
                                </span>
                                </c:if>
                                <c:if test="${sessionScope.commentPb.totalCount == 0}">
                                <span style="font-size: 25px;margin-left: 5px;">
                                    Have no comment!
                                </span>
                                </c:if>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>

</main>
<!--End Main page content-->
<script type="text/javascript" src="${pageContext.request.contextPath }/js/manager/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath }/js/manager/bootstrap.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath }/js/manager/perfect-scrollbar.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath }/js/manager/main.min.js"></script>
<script src="${pageContext.request.contextPath }/js/manager/bootstrap-notify.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath }/js/manager/tips.js"></script>
<%-- Tag insertion --%>
<script src="${pageContext.request.contextPath }/js/manager/jquery-tags-input/jquery.tagsinput.min.js"></script>
<script src="${pageContext.request.contextPath }/js/jquerysession.js"></script>
<script type="text/javascript">
    $(function(){
        $('.search-bar .dropdown-menu a').click(function() {
            var field = $(this).data('field') || '';
            $('#search-field').val(field);
            $('#search-btn').html($(this).text() + ' <span class="caret"></span>');
            $("#keyName").attr("placeholder", "Input" + $(this).text());
        });

        $('#keyName').blur(function (){
            var keyValue = $("#keyName").val();
            if(keyValue == null || keyValue == ""){
                return false;
            }else {
                window.location.href = "${pageContext.request.contextPath}/admin/findCommentByPageServlet?rows=6&" + $("#search-field").val() + "=" + keyValue;
            }
        });

    });

    function updateComment(id){
        const v = "#updateContext_" + id;
        const context = $(v).val();
        if(context == null || context == "") {
            alert("Comment Content Cannot Null！！！~");
            return false;
        }
        if(confirm("Are you sure to update?")) {
            let str = "#form_update_" + id;
            $(str).submit();
        }
    }

    function deleteComment(id){
        if(confirm("Are you sure to delete?")){
            //location path
            location.href="${pageContext.request.contextPath}/admin/deleteCommentServlet?commentId="+id;
        }
    }

    function delSelected(){
        if(confirm("Are you sure to delete this?")){
            let flag = false;
            // Judge if its selected
            let checks = document.getElementsByName("commentId");
            let str = "";
            for(let i = 0; i < checks.length; i++){
                if(checks[i].checked){
                    flag = true;
                    str += "commentId=" + checks[i].value + "&";
                    // break;
                }
            }

            if(flag) {
                // $("#form_del").submit();
                window.location.href = "${pageContext.request.contextPath}/admin/deleteCommentServlet?" + str;
            }else {
                return false;
            }
        }
    }

</script>
</body>
</html>
