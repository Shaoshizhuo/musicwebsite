<%--
  Created by IntelliJ IDEA.
  Creater: Shao Shizhuo
  Date: 2023/1/10
  Time: 19:33
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
    <title>Add new song</title>
    <link rel="icon" href="favicon.ico" type="image/ico">
    <link href="${pageContext.request.contextPath }/css/manager/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath }/css/manager/fonts.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath }/css/manager/style.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath }/css/manager/animate.css" rel="stylesheet">
    <link rel="stylesheet" href="${pageContext.request.contextPath }/js/manager/jquery-tags-input/jquery.tagsinput.min.css">
</head>

<body>
<!--Main-->
<main class="ftdms-layout-content">

    <div class="container-fluid">

        <div class="row" style="margin-top:15px;">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header"><h4>Guide</h4></div>
                    <div class="card-body">

                        <form action="<%=request.getContextPath()%>/admin/uploadFileServlet?currentPage=${sessionScope.pb.currentPage + 1}&rows=6&name=${sessionScope.condition.song_name[0]}&singerName=${sessionScope.condition.singer_name[0]}&type=${sessionScope.condition.song_type[0]}" method="post" onsubmit="return submitFunc(this)" enctype="multipart/form-data" class="guide-box" data-navigateable="true">
                            <ul class="nav-step step-dots">
                                <li class="nav-step-item active">
                                    <span>Song & Singer Name</span>
                                    <a class="active" data-toggle="tab" href="#step-1"></a>
                                </li>

                                <li class="nav-step-item">
                                    <span>Language & Type</span>
                                    <a data-toggle="tab" href="#step-2"></a>
                                </li>

                                <li class="nav-step-item">
                                    <span>Song File</span>
                                    <a data-toggle="tab" href="#step-3"></a>
                                </li>

                                <li class="nav-step-item">
                                    <span>Lyrics</span>
                                    <a data-toggle="tab" href="#step-4"></a>
                                </li>
                            </ul>
                            <!--Corresponding content-->
                            <div class="nav-step-content">
                                <div class="nav-step-pane hidden active" id="step-1">
                                    <div class="form-group">
                                        <label for="songName" class="control-label">Song Name：</label>
                                        <input type="text" class="form-control" id="songName" name="songName">
                                    </div>
                                    <div class="form-group">
                                        <label for="singerName" class="control-label">Singer Name：</label>
                                        <input type="text" class="form-control" id="singerName" name="singerName">
                                    </div>
                                </div>

                                <div class="nav-step-pane hidden" id="step-2">
                                    <div class="form-group">
                                        <label for="language" class="control-label">Language：</label>
                                        <input type="text" class="form-control" id="language" name="language">
                                    </div>
                                    <div class="form-group">
                                        <label for="songType" class="control-label">Type：</label>
                                        <input class="form-control js-tags-input" type="text" id="songType" name="songType" placeholder="Sel/Input" value="Pop,Rock">
                                    </div>
                                </div>

                                <div class="nav-step-pane hidden" id="step-3">
                                    <div class="form-group">
                                        <label for="musicFile" class="control-label">Song File：</label>
                                        <input type="file" id="musicFile" name="musicFile">
                                    </div>
                                </div>

                                <div class="nav-step-pane hidden" id="step-4">
                                    <div class="form-group">
                                        <label for="lyric" class="control-label">Lyrics：</label>
                                        <textarea class="form-control" id="lyric" name="lyric"></textarea>
                                    </div>
                                </div>
                            </div>
                            <!--End Corresponding content-->
                            <hr>
                            <div class="nav-step-button">
                                <button class="btn btn-secondary disabled" data-wizard="prev" type="button">Previous</button>
                                <button class="btn btn-secondary" data-wizard="next" type="button">Next</button>
                                <button class="btn btn-primary hidden" data-wizard="finish" type="submit">Finish</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>

        </div>

    </div>

</main>
<!--End Main page content-->
</div>
</div>

<script type="text/javascript" src="${pageContext.request.contextPath }/js/manager/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath }/js/manager/bootstrap.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath }/js/manager/main.min.js"></script>
<script src="${pageContext.request.contextPath }/js/manager/bootstrap-notify.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath }/js/manager/tips.js"></script>
<%-- Tag insertion --%>
<script src="${pageContext.request.contextPath }/js/manager/jquery-tags-input/jquery.tagsinput.min.js"></script>
<!-- Guide insertion -->
<script type="text/javascript" src="${pageContext.request.contextPath }/js/manager/jquery.bootstrap.wizard.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.guide-box').bootstrapWizard({
            'tabClass': 'nav-step',
            'nextSelector': '[data-wizard="next"]',
            'previousSelector': '[data-wizard="prev"]',
            'finishSelector': '[data-wizard="finish"]',
            'onTabClick': function(e, t, i) {
                if (!$('.guide-box').is('[data-navigateable="true"]')) return ! 1
            },
            'onTabShow': function(e, t, i) {
                t.children(":gt(" + i + ").complete").removeClass("complete");
                t.children(":lt(" + i + "):not(.complete)").addClass("complete");
            },
            'onFinish': function(e, t, i) {
                // return submitFunc();
            }
        });
    });

    function submitFunc(e) {
        tips.loading("show");
        var songName = $("#songName").val();
        var singerName = $("#singerName").val();
        var language = $("#language").val();
        var songType = $("#songType").val();
        var lyric = $("#lyric").val();
        var musicFile = $('#musicFile').get(0).files[0];

        if (songName == null || songName == "") {
            tips.loading("hide");
            tips.notify('Song Name Cannot Null~', 'danger', 100);
            return false;
        }
        if (singerName == null || singerName == "") {
            tips.loading("hide");
            tips.notify('Singer Name Cannot Null~', 'danger', 100);
            return false;
        }
        if (language == null || language == "") {
            tips.loading("hide");
            tips.notify('Language Cannot Null~', 'danger', 100);
            return false;
        }
        if (songType == null || songType == "") {
            tips.loading("hide");
            tips.notify('Type Cannot Null~', 'danger', 100);
            return false;
        }
        if (lyric == null || lyric == "") {
            tips.loading("hide");
            tips.notify('Lyrics Cannot Null~', 'danger', 100);
            return false;
        }
        if (!musicFile) {
            tips.loading("hide");
            tips.notify('File Cannot Null~', 'danger', 100);
            return false;
        }
        var pathUrl = getPath() + "/admin/addSongServlet";
        var uploadUrl = getPath() + "/admin/uploadFileServlet";
        var formatData = new FormData();
        formatData.append("musicFile", musicFile);
        var jsonData = {
            "songName": songName,
            "singerName": singerName,
            "language": language,
            "songType": songType,
            "lyric": lyric,
        }
    }
</script>
</body>
</html>