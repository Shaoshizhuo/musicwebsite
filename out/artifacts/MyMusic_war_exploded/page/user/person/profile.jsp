
<%@ include file="../_pre.jsp"%>
<%--
  Created by IntelliJ IDEA.
  Creater: Shao Shizhuo
  Date: 2023/1/25
  Time: 10:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="${pageContext.request.contextPath}/page/user/assets/img/basic/favicon.ico" type="image/x-icon">
    <title>Personal Information</title>
    <!-- CSS -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/page/user/assets/css/app.css">
</head>

<body class="sidebar-mini sidebar-collapse theme-dark  sidebar-expanded-on-hover has-preloader" style="display: none;">

<div id="app">
<!--Page Content-->
<main id="pageContent" class="page has-sidebar">
    <div class="container-fluid relative animatedParent animateOnce p-lg-5">
        <div class="card no-b shadow no-r">
            <div class="row no-gutters">
                <div class="col-md-4 b-r">
                    <div class="text-center p-5 mt-5">
                        <figure class="avatar avatar-xl">
                            <img src="${pageContext.request.contextPath }/${sessionScope.user.avatar}" alt=""></figure>
<%--                            <img src="${pageContext.request.contextPath + sessionScope.user.avatar }" alt=""></figure>--%>
                        <div>
                            <h4 class="p-t-10">${sessionScope.user.name}</h4>
                            ${sessionScope.user.email}
                        </div>
                        <a href="#" class="btn btn-outline-primary btn-sm  mt-3">Modify</a>
                        <div class="mt-5">
                            <ul class="social social list-inline">
                                <li class="list-inline-item"><a href="#" class="grey-text"><i class="icon-facebook"></i></a>
                                </li>
                                <li class="list-inline-item"><a href="#" class="grey-text"><i class="icon-youtube"></i></a>
                                </li>
                                <li class="list-inline-item"><a href="#" class="grey-text"><i class="icon-twitter"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="p5 b-b">
                        <div class="pl-4 mt-4">
                            <h5>Official Information</h5>
                        </div>
                        <div class="row">
                            <div class="col-md-5">
                                <div class="p-4">
                                    <h4>Username</h4>
                                    <span>${sessionScope.user.username}</span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="p-4">
                                    <h4>Email</h4>
                                    <span>${sessionScope.user.email}</span>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="p-4">
                                    <h4>Phone</h4>
                                    <span>${sessionScope.user.phone}</span>
                                </div>
                            </div>


                        </div>
                    </div>
                    <div class="p5 b-b">
                        <div class="pl-4 mt-4">
                            <h5>Personal Information</h5>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="p-4">
                                    <h4>Birthday</h4>
                                    <span>${sessionScope.user.birth}</span>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="p-4">
                                    <h4>User Type</h4>
                                    <span>${sessionScope.user.type == 0 ? "Normal" : "VIP(" + sessionScope.user.vipDate + "Expire）"}</span>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="p-4">
                                    <h4>Signature</h4>
                                    <span>
                                        <img src="${pageContext.request.contextPath}/${sessionScope.user.sign}" alt="">
                                    </span>

                                </div>
                            </div>

                        </div>
                    </div>

                    <%--<div class="p-4">
                        <h4>New following</h4>
                        <ul class="list-inline mt-3">
                            <li class="list-inline-item ">
                                <img src="${pageContext.request.contextPath }/page/user/assets/img/demo/u13.png" alt="" class="img-responsive w-40px circle mb-3">
                            </li>
                            <li class="list-inline-item">
                                <img src="${pageContext.request.contextPath }/page/user/assets/img/demo/u12.png" alt="" class="img-responsive w-40px circle mb-3">
                            </li>
                            <li class="list-inline-item">
                                <img src="${pageContext.request.contextPath }/page/user/assets/img/demo/u11.png" alt="" class="img-responsive w-40px circle mb-3">
                            </li>
                            <li class="list-inline-item">
                                <img src="${pageContext.request.contextPath }/page/user/assets/img/demo/u10.png" alt="" class="img-responsive w-40px circle mb-3">
                            </li>
                            <li class="list-inline-item">
                                <img src="${pageContext.request.contextPath }/page/user/assets/img/demo/u9.png" alt="" class="img-responsive w-40px circle mb-3">
                            </li>
                            <li class="list-inline-item">
                                <img src="${pageContext.request.contextPath }/page/user/assets/img/demo/u8.png" alt="" class="img-responsive w-40px circle mb-3">
                            </li>
                            <li class="list-inline-item ">
                                <img src="${pageContext.request.contextPath }/page/user/assets/img/demo/u7.png" alt="" class="img-responsive w-40px circle mb-3">
                            </li>
                            <li class="list-inline-item">
                                <img src="${pageContext.request.contextPath }/page/user/assets/img/demo/u6.png" alt="" class="img-responsive w-40px circle mb-3">
                            </li>
                            <li class="list-inline-item">
                                <img src="${pageContext.request.contextPath }/page/user/assets/img/demo/u5.png" alt="" class="img-responsive w-40px circle mb-3">
                            </li>
                            <li class="list-inline-item">
                                <img src="${pageContext.request.contextPath }/page/user/assets/img/demo/u4.png" alt="" class="img-responsive w-40px circle mb-3">
                            </li>
                            <li class="list-inline-item">
                                <img src="${pageContext.request.contextPath }/page/user/assets/img/demo/u3.png" alt="" class="img-responsive w-40px circle mb-3">
                            </li>
                            <li class="list-inline-item">
                                <img src="${pageContext.request.contextPath }/page/user/assets/img/demo/u2.png" alt="" class="img-responsive w-40px circle mb-3">
                            </li>
                        </ul>
                    </div>--%>

                </div>
            </div>

        </div>

    </div>




</main><!--@Page Content-->
</div><!--@#app-->
<!--/#app -->
<script src="${pageContext.request.contextPath }/page/user/assets/js/app.js"></script>


</body>
</html>
