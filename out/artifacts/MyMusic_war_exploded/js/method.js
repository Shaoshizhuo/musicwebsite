
!function (win, doc, undefined) {

    var Progressbar = function ($progressbar, $progressbarCur, $progressbarCurDot) {

        this.progressbar = $progressbar;
        this.progressbarCur = $progressbarCur;
        this.progressbarCurDot = $progressbarCurDot;
        this.curDotWidth = this.progressbarCurDot.outerWidth();
        this.barOffsetWidth = this.progressbar.outerWidth();
        this.barOffsetLeft = this.progressbar.offset().left;
        this.isMove = true;
    }

    Progressbar.prototype = {

        constructor: Progressbar,

        progressbarClick: function (callBack) {
            var _this = this;
            this.progressbar.on('click', function (e) {
                var resetRatio = (e.pageX - _this.barOffsetLeft) / _this.barOffsetWidth;
                _this.setProgressCurSite(resetRatio);
                callBack && callBack(resetRatio);
            });

        },

        setProgressCurSite: function (value) {
            var barOffsetWidth = this.barOffsetWidth,
                tarVal = value * barOffsetWidth * (barOffsetWidth - this.curDotWidth) / barOffsetWidth;
            this.progressbarCur.css('width', tarVal + 'px');
            this.progressbarCurDot.css('left', tarVal + 'px');
        },

        progressbarDrag: function (callBack, callBackVoice) {
            var _this = this,
                $doc = $('html,body');
            this.progressbar.on('mousedown', function (e) {

                if (_this.isMove && e.button === 0) {
                    _this.isMove = false;
                    var tarPointRatio = (e.pageX - _this.barOffsetLeft) / _this.barOffsetWidth;
                    _this.setProgressCurSite(tarPointRatio);
                    $doc.on('mousemove', function (e) {
                        tarPointRatio = (e.pageX - _this.barOffsetLeft) / _this.barOffsetWidth;
                        if (tarPointRatio < 0) {
                            tarPointRatio = 0;
                        }
                        if (tarPointRatio > 1) {
                            tarPointRatio = 1;
                        }
                        _this.setProgressCurSite(tarPointRatio);
                        callBackVoice && callBackVoice(tarPointRatio);
                    });

                    $doc.on('mouseup', function () {
                        $doc.off('mousemove');
                        callBack && callBack(tarPointRatio);
                        _this.isMove = true;
                        $doc.off('mouseup');
                    });
                }

            })


        }

    }

    window.Progressbar = Progressbar;

}(window, document);



!function (win, doc, undefined) {

    var Player = function (data, audio, params) {

        this.audio = audio;
        this.data = data;
        this.$byOrder = params.$byOrder;
        this.playIndex = 0;
        this.clickJumpPlay = false;

    }

    Player.prototype = {

        constructor: Player,

        formatTime: function (currentTime, duration) {

            var curMinus = parseInt(currentTime / 60),
                curSecond = parseInt(currentTime - curMinus * 60),
                fixedMinus = parseInt(duration / 60),
                fixedSecond = parseInt(duration - fixedMinus * 60);

            curMinus < 10 && (curMinus = '0' + curMinus);
            curSecond < 10 && (curSecond = '0' + curSecond);
            fixedMinus < 10 && (fixedMinus = '0' + fixedMinus);
            fixedSecond < 10 && (fixedSecond = '0' + fixedSecond);

            curFormatTime = curMinus + ':' + curSecond;
            fixedFormatTime = fixedMinus + ':' + fixedSecond;
            return curFormatTime;

        },


        timeProgressListener: function (callBack) {
            var _this = this;
            _this.audio.addEventListener('timeupdate', function () {
                var playRatio = this.currentTime / this.duration,
                    timeStr = _this.formatTime(this.currentTime, this.duration);
                callBack(playRatio, timeStr);
            }, false);
        },


        setPlayPaused: function (arr, option) {
            if (this.audio.paused) {
                arr.forEach(function (e) {
                    e.removeClass(option.paused).addClass(option.played);
                    e.attr('title','暂停');
                });
                this.audio.play();
            } else {
                arr.forEach(function (e) {
                    e.removeClass(option.played).addClass(option.paused);
                    e.attr('title','播放');
                });
                this.audio.pause();
            }
        },


        setPlayVoice: function (value) {
            this.audio.volume = value * 1;
        },

        setPlayTime: function (value) {
            var curTime = value * this.audio.duration;
            this.audio.currentTime = curTime;

        },

        autoSwitchLogic: function (option) {
            var dataLen = this.data.length;
            switch (true) {
                case this.$byOrder.hasClass(option.forwardPlay):
                    this.playIndex++;
                    this.playIndex >= dataLen && (this.playIndex = 0);
                    break;
                case this.$byOrder.hasClass(option.randomPlay):
                    var randomNum = Math.round(Math.random() * (dataLen - 1)),
                        playIndex = this.playIndex;
                    this.playIndex = playIndex == randomNum ? ++playIndex : randomNum;
                    this.playIndex >= dataLen && (this.playIndex = 0);
                    break;
                case this.$byOrder.hasClass(option.circulate):
                    break;
            }

        },

        clickSwitchLogic: function (option) {
            var dataLen = this.data.length;
            switch (true) {
                case this.$byOrder.hasClass(option.randomPlay):
                    var randomNum = Math.round(Math.random() * (dataLen - 1)),
                        playIndex = this.playIndex;
                    this.playIndex = playIndex == randomNum ? ++playIndex : randomNum;
                    this.playIndex >= dataLen && (this.playIndex = 0);
                    break;
                case option.whichWayPlay === 'next':
                    this.playIndex++;
                    this.playIndex >= dataLen && (this.playIndex = 0);
                    break;
                case option.whichWayPlay === 'prev':
                    this.playIndex--;
                    this.playIndex < 0 && (this.playIndex = dataLen - 1);
                    break;
            }
        },

        lrcLiClickSwitchEffect: function (curItem, setPlayPaused, tabNext) {
            var curClickIndex = curItem.index();
            if (this.playIndex === curClickIndex) {
                setPlayPaused();
            } else {
                this.playIndex = curClickIndex;
                tabNext();
            }
        }

    }

    window.Player = Player;

}(window, document);




!function (win, doc, undefined) {

    var Lyric = function (lyricFile, lyricBox, curLiClass) {
        this.lyricFile = lyricFile;
        this.lyricBox = lyricBox;
        this.curLiClass = curLiClass;
        this.isMouseDown = false;
        this.limitValue = -1;
        this.allLiTopArr = [];
        this.allLiHeightArr = [];
        this.getLyricList();
    }

    Lyric.prototype = {

        constructor: Lyric,
        init: function (lyricData) {
            this.lyricStr = this.parseLyricInfo(lyricData);
            this.appendLyric(this.lyricStr.str);
            this.allLyricLi = this.lyricBox.children();
            this.initTopVal = (this.lyricBox.parent().outerHeight() - this.allLyricLi.eq(0).outerHeight()) / 2;
            this.lyricBox.css('top', this.initTopVal + 'px');
            this.getLyricLiTop(this.allLyricLi);
            this.initLyricBoxSite();
        },


        getLyricLiTop: function (allLyricLi) {
            var _this = this,
                flagVal = 0;
            _this.allLiTopArr.push(flagVal);
            allLyricLi.each(function (i, e) {
                var $eHeight = $(e).outerHeight();
                flagVal -= $eHeight;
                _this.allLiTopArr.push(flagVal);
                _this.allLiHeightArr.push($eHeight);
            });
            _this.allLiTopArr = _this.allLiTopArr.slice(0, _this.allLiTopArr.length - 1);
        },

        getLyricList: function () {
            var _this = this;
            $.ajax({

                url: _this.lyricFile,

                type: 'get',

                dataType: 'text',

                success: function (lyricData) {

                    _this.init(lyricData);

                },
                error: function (e) {
                    console.log(e);
                }


            })

        },

        parseLyricInfo: function (lyricData) {
            var _this = this,
                lyricReg = /\[(\d*:\d*.\d*)\]/,
                lyricArr = lyricData.split('\n');

            var curLyricArr = [], curTimeArr = [], listHtml = '';
            $.each(lyricArr, function (i, e) {
                if (lyricReg.test(e)) {

                    var separate = e.split(']'),
                        separateBefore = separate[0].slice(1),
                        separateAfter = separate[1];

                    if (separateAfter !== undefined && Number(separateAfter) !== 0) {
                        listHtml = '<li>' + separateAfter + '</li>\n';
                        curLyricArr.push(listHtml);
                        curTimeArr.push(_this.parseLyricTime(separateBefore));
                    }
                }
            });

            return { 'str': curLyricArr, 'time': curTimeArr };
        },

        parseLyricTime: function (timeNode) {
            var formatTimeNode = timeNode.split(':'),
                formatMinute = Number(formatTimeNode[0]) * 60,
                formatSecond = parseFloat(Number(formatTimeNode[1]).toFixed(1)),
                activeTime = formatMinute + formatSecond;
            return activeTime;
        },

        appendLyric: function (listHtml) {
            this.lyricBox.html(listHtml.join(''));
        },

        initLyricBoxSite: function () {
            this.lyricBox.css({
                'marginTop':0
            });
        },

        shiftLyricBox: function (curTime, upperLimt) {

            if (this.lyricStr !== undefined) {
                curTime = parseFloat(curTime.toFixed(1));
                var _this = this,
                    // nextTime = 0,
                    lrcTimeStr = jQuery.extend(true, [], _this.lyricStr.time),
                    lrcTimeStrLen = lrcTimeStr.length - 1;
                lrcTimeStr.push(upperLimt);

                var curTimeVal = lrcTimeStr.filter(function (e, i) {
                    if (i > lrcTimeStrLen) return;
                    return curTime >= e && curTime < lrcTimeStr[i + 1];
                })[0];

                if (_this.limitValue != curTimeVal && !_this.isMouseDown && curTimeVal !== undefined) {
                    _this.limitValue = curTimeVal;
                    var curIndex = lrcTimeStr.indexOf(curTimeVal);
                    _this.allLyricLi.eq(curIndex).addClass(_this.curLiClass).siblings().removeClass(_this.curLiClass);
                    _this.lyricBox.css({
                        'transition':'.3s',
                        'marginTop': _this.allLiTopArr[curIndex] + 'px'
                    });
                }

                // var curIndex = lrcTimeStr.findIndex(function (e, i) {
                //     return curTime >= e && curTime < lrcTimeStr[i + 1];
                // });

                // $.each(lrcTimeStr, function (i, e) {
                //     if(i > lrcTimeStrLen) return;
                //     nextTime = lrcTimeStr[i + 1];

                //     if (curTime >= e && curTime < nextTime && !_this.isMouseDown) {
                //         if (_this.limitValue != e) {
                //             _this.limitValue = e;
                //              _this.allLyricLi.eq(i).addClass(_this.curLiClass).siblings().removeClass(_this.curLiClass);
                //             _this.lyricBox.animate({
                //                 'marginTop':_this.allLiTopArr[i] + 'px'
                //             },100);
                //         }
                //     }
                // });
            }
        },


        getCurLiIndex: function (initVal, tarArr) {
            var tarArrLen = tarArr.length - 1,
                resetTarArr = jQuery.extend(true, [], tarArr);
            resetTarArr.push(tarArr[tarArrLen] - 1);
            for (var item = 0; item <= tarArrLen; item++) {
                if (initVal <= resetTarArr[item] && initVal > resetTarArr[item] - this.allLiHeightArr[item]) {
                    return item;
                }
            }
        },

        getAdaptTime: function (moveY) {
            var targetIndex = this.getCurLiIndex(moveY, this.allLiTopArr),
                activeLrcTime = this.lyricStr.time[targetIndex],
                activeLrcMin = Math.floor(activeLrcTime / 60),
                activeLrcSecond = Math.round(activeLrcTime % 60);
            activeLrcMin < 10 && (activeLrcMin = '0' + activeLrcMin);
            activeLrcSecond < 10 && (activeLrcSecond = '0' + activeLrcSecond);
            return { 'secondsTime': activeLrcTime, 'formatTime': activeLrcMin + ':' + activeLrcSecond };
        }
    }

    window.Lyric = Lyric;

}(window, document);