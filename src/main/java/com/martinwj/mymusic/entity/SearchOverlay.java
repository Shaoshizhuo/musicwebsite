package com.martinwj.mymusic.entity;

import java.util.List;
import java.util.Map;

/**
 * @ClassName: SearchOverlay
 * @Description: TODO
 * @author: Shao Shizhuo
 * @createDate: 2023-1-20
 *
 */
public class SearchOverlay {

    private String info;
    private Map<Singer, Integer> Map;
    private List<Song> list;

    public SearchOverlay(){}

    public SearchOverlay(String info, java.util.Map<Singer, Integer> map, List<Song> list) {
        this.info = info;
        Map = map;
        this.list = list;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public java.util.Map<Singer, Integer> getMap() {
        return Map;
    }

    public void setMap(java.util.Map<Singer, Integer> map) {
        Map = map;
    }

    public List<Song> getList() {
        return list;
    }

    public void setList(List<Song> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "SearchOverlay{" +
                "info='" + info + '\'' +
                ", Map=" + Map +
                ", list=" + list +
                '}';
    }
}
