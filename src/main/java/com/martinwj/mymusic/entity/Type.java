package com.martinwj.mymusic.entity;

/**
 * @ClassName: Type
 * @Description: TODO
 * @author: Shao Shizhuo
 * @createDate: 2023-1-25
 *
 */
public class Type {
    private Integer typeId;
    private String name;
    private int songCount;

    public Type() {
    }

    public Type(Integer typeId, String name, int songCount) {
        this.typeId = typeId;
        this.name = name;
        this.songCount = songCount;
    }

    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSongCount() {
        return songCount;
    }

    public void setSongCount(int songCount) {
        this.songCount = songCount;
    }

    @Override
    public String toString() {
        return "Type{" +
                "typeId=" + typeId +
                ", name='" + name + '\'' +
                ", songCount=" + songCount +
                '}';
    }
}
