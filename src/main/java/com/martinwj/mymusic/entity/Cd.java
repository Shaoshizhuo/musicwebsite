package com.martinwj.mymusic.entity;

/**
 * @ClassName: Cd
 * @Description: TODO
 * @author: Shao Shizhuo
 * @createDate: 2023-1-20
 *
 */
public class Cd {

    private int cdId;
    private String name;
    private String coverUrl;
    private int num;
    private String publicDate;
    private int singerId;
    private String introduction;
    private int collectionCount;

    public Cd() {

    }

    public Cd(int cdId, String name, String coverUrl, int num, String publicDate, int singerId, String introduction, int collectionCount) {
        this.cdId = cdId;
        this.name = name;
        this.coverUrl = coverUrl;
        this.num = num;
        this.publicDate = publicDate;
        this.singerId = singerId;
        this.introduction = introduction;
        this.collectionCount = collectionCount;
    }

    public int getCdId() {
        return cdId;
    }

    public void setCdId(int cdId) {
        this.cdId = cdId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCoverUrl() {
        return coverUrl;
    }

    public void setCoverUrl(String coverUrl) {
        this.coverUrl = coverUrl;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getPublicDate() {
        return publicDate;
    }

    public void setPublicDate(String publicDate) {
        this.publicDate = publicDate;
    }

    public int getSingerId() {
        return singerId;
    }

    public void setSingerId(int singerId) {
        this.singerId = singerId;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public int getCollectionCount() {
        return collectionCount;
    }

    public void setCollectionCount(int collectionCount) {
        this.collectionCount = collectionCount;
    }

    @Override
    public String toString() {
        return "Cd{" +
                "cdId=" + cdId +
                ", name='" + name + '\'' +
                ", coverUrl='" + coverUrl + '\'' +
                ", num=" + num +
                ", publicDate='" + publicDate + '\'' +
                ", singerId=" + singerId +
                ", introduction='" + introduction + '\'' +
                ", collectionCount=" + collectionCount +
                '}';
    }
}
