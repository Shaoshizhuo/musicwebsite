package com.martinwj.mymusic.entity;

/**
 * @ClassName: SongList
 * @Description: TODO
 * @author: Shao Shizhuo
 * @createDate: 2023-1-23
 *
 */
public class SongList {

    private int songListId;
    private String name;
    private int userId;
    private int accessCount;
    private String introduction;
    private int collectionCount;
    private String tags;
    private String photoUrl;
    private String createDate;
    private String updateDate;

    public SongList() {

    }

    public SongList(int songListId, String name, int userId, int accessCount, String introduction, int collectionCount, String tags, String photoUrl, String createDate, String updateDate) {
        this.songListId = songListId;
        this.name = name;
        this.userId = userId;
        this.accessCount = accessCount;
        this.introduction = introduction;
        this.collectionCount = collectionCount;
        this.tags = tags;
        this.photoUrl = photoUrl;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }

    public int getSongListId() {
        return songListId;
    }

    public void setSongListId(int songListId) {
        this.songListId = songListId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getAccessCount() {
        return accessCount;
    }

    public void setAccessCount(int accessCount) {
        this.accessCount = accessCount;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public int getCollectionCount() {
        return collectionCount;
    }

    public void setCollectionCount(int collectionCount) {
        this.collectionCount = collectionCount;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    @Override
    public String toString() {
        return "SongList{" +
                "songListId=" + songListId +
                ", name='" + name + '\'' +
                ", userId=" + userId +
                ", accessCount=" + accessCount +
                ", introduction='" + introduction + '\'' +
                ", collectionCount=" + collectionCount +
                ", tags='" + tags + '\'' +
                ", photoUrl='" + photoUrl + '\'' +
                ", createDate='" + createDate + '\'' +
                ", updateDate='" + updateDate + '\'' +
                '}';
    }
}
