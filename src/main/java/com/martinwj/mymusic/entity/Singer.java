package com.martinwj.mymusic.entity;

import java.util.Date;

/**
 * @ClassName: Singer
 * @Description: TODO
 * @author: Shao Shizhuo
 * @createDate: 2023-1-22
 *
 */
public class Singer {

    private int singerId;
    private String name;
    private String englishName;
    private int sex;
    private String pic;
    private String birth;
    private String location;
    private String introduction;
    private int accessCount;
    private int collectionCount;
    private String debutDate;

    public Singer() {
    }

    public Singer(int singerId, String name, String englishName, int sex, String pic, String birth, String location, String introduction, int accessCount, int collectionCount, String debutDate) {
        this.singerId = singerId;
        this.name = name;
        this.englishName = englishName;
        this.sex = sex;
        this.pic = pic;
        this.birth = birth;
        this.location = location;
        this.introduction = introduction;
        this.accessCount = accessCount;
        this.collectionCount = collectionCount;
        this.debutDate = debutDate;
    }

    public int getSingerId() {
        return singerId;
    }

    public void setSingerId(int singerId) {
        this.singerId = singerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getBirth() {
        return birth;
    }

    public void setBirth(String birth) {
        this.birth = birth;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public int getAccessCount() {
        return accessCount;
    }

    public void setAccessCount(int accessCount) {
        this.accessCount = accessCount;
    }

    public int getCollectionCount() {
        return collectionCount;
    }

    public void setCollectionCount(int collectionCount) {
        this.collectionCount = collectionCount;
    }

    public void setDebutDate(String debutDate) {
        this.debutDate = debutDate;
    }

    public String getLocation() {
        return location;
    }

    public String getEnglishName() {
        return englishName;
    }

    public void setEnglishName(String englishName) {
        this.englishName = englishName;
    }

    public String getDebutDate() {
        return debutDate;
    }

    @Override
    public String toString() {
        return "Singer{" +
                "singerId=" + singerId +
                ", name='" + name + '\'' +
                ", englishName='" + englishName + '\'' +
                ", sex=" + sex +
                ", pic='" + pic + '\'' +
                ", birth='" + birth + '\'' +
                ", location='" + location + '\'' +
                ", introduction='" + introduction + '\'' +
                ", accessCount=" + accessCount +
                ", collectionCount=" + collectionCount +
                ", debutDate='" + debutDate + '\'' +
                '}';
    }
}
