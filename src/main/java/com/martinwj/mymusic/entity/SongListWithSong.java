package com.martinwj.mymusic.entity;

/**
 * @ClassName: SongListWithSong
 * @Description: TODO
 * @author: Shao Shizhuo
 * @createDate: 2023-1-23
 *
 */
public class SongListWithSong {

    private int id;
    private int songListId;
    private int songId;
    private String createDate;

    public SongListWithSong(){}

    public SongListWithSong(int id, int songListId, int songId, String createDate) {
        this.id = id;
        this.songListId = songListId;
        this.songId = songId;
        this.createDate = createDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSongListId() {
        return songListId;
    }

    public void setSongListId(int songListId) {
        this.songListId = songListId;
    }

    public int getSongId() {
        return songId;
    }

    public void setSongId(int songId) {
        this.songId = songId;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    @Override
    public String toString() {
        return "SongListWithSong{" +
                "id=" + id +
                ", songListId=" + songListId +
                ", songId=" + songId +
                ", createDate='" + createDate + '\'' +
                '}';
    }
}
