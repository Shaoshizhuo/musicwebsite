package com.martinwj.mymusic.entity;

import java.util.List;

/**
 * @ClassName: UserSaved
 * @Description: TODO
 * @author: Shao Shizhuo
 * @createDate: 2023-1-25
 *
 */
public class UserSaved {

    private SongListWithSong songListWithSong;

    private SongList songList;

    private User user;

    private List<Song> list;

    public UserSaved() {
    }

    public UserSaved(SongListWithSong songListWithSong, SongList songList, User user, List<Song> list) {
        this.songListWithSong = songListWithSong;
        this.songList = songList;
        this.user = user;
        this.list = list;
    }

    public SongListWithSong getSongListWithSong() {
        return songListWithSong;
    }

    public void setSongListWithSong(SongListWithSong songListWithSong) {
        this.songListWithSong = songListWithSong;
    }

    public SongList getSongList() {
        return songList;
    }

    public void setSongList(SongList songList) {
        this.songList = songList;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Song> getList() {
        return list;
    }

    public void setList(List<Song> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "UserSaved{" +
                "songListWithSong=" + songListWithSong +
                ", songList=" + songList +
                ", user=" + user +
                ", list=" + list +
                '}';
    }
}
