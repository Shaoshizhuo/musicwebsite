package com.martinwj.mymusic.entity;

import java.util.List;
import java.util.Map;

/**
 * @ClassName: CommentWithSong
 * @Description: TODO
 * @author: Shao Shizhuo
 * @createDate: 2023-1-20
 *
 */
public class CommentWithSong {

    private int totalCount;
    private int totalPage;
    private int currentPage;
    private int rows;

    private Song song;

    private Map<User, Comment> commentMap;

    private Singer singer;

    public CommentWithSong(){}

    public CommentWithSong(int totalCount, int totalPage, int currentPage, int rows, Song song, Map<User, Comment> commentMap, Singer singer) {
        this.totalCount = totalCount;
        this.totalPage = totalPage;
        this.currentPage = currentPage;
        this.rows = rows;
        this.song = song;
        this.commentMap = commentMap;
        this.singer = singer;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getRows() {
        return rows;
    }

    public void setRows(int rows) {
        this.rows = rows;
    }

    public Song getSong() {
        return song;
    }

    public void setSong(Song song) {
        this.song = song;
    }

    public Map<User, Comment> getCommentMap() {
        return commentMap;
    }

    public void setCommentMap(Map<User, Comment> commentMap) {
        this.commentMap = commentMap;
    }

    public Singer getSinger() {
        return singer;
    }

    public void setSinger(Singer singer) {
        this.singer = singer;
    }

    @Override
    public String toString() {
        return "CommentWithSong{" +
                "totalCount=" + totalCount +
                ", totalPage=" + totalPage +
                ", currentPage=" + currentPage +
                ", rows=" + rows +
                ", song=" + song +
                ", commentMap=" + commentMap +
                ", singer=" + singer +
                '}';
    }
}
