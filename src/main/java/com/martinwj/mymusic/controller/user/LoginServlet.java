package com.martinwj.mymusic.controller.user;

import com.martinwj.mymusic.entity.User;
import com.martinwj.mymusic.service.UserService;
import com.martinwj.mymusic.service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @ClassName: loginServlet
 * @Description: TODO
 * @author: Shao Shizhuo
 * @createDate: 2023-1-28
 *
 */
@WebServlet("/user/loginServlet")
public class LoginServlet extends HttpServlet {

    private UserService userService = null;

    @Override
    public void init() throws ServletException {
        userService = new UserServiceImpl();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        User user = userService.getUserByLogin(username, password);
        if(user == null) {
            request.setAttribute("titleMsg", "Login failure！");
            request.setAttribute("textMsg", "The page is missing, please try again later");
            String url = request.getContextPath() + "/page/user/login.jsp";
            request.setAttribute("urlMsg", url);
            request.setAttribute("pageMsg", "Login");
            request.setAttribute("codeMsg", "404");
            request.getRequestDispatcher("/page/user/message.jsp").forward(request, response);
        } else if(user.getActive() == 0) {
            request.setAttribute("titleMsg", "Login failure！");
            request.setAttribute("textMsg", "You haven't activated your email yet. Please activate it and try again！");
            String url = request.getContextPath() + "/page/user/login.jsp";
            request.setAttribute("urlMsg", url);
            request.setAttribute("pageMsg", "Login");
            request.setAttribute("codeMsg", "");
            request.getRequestDispatcher("/page/user/message.jsp").forward(request, response);
        } else if(user.getActive() == 1) {
            // Login successfully, jump to the main page
            // Store user information into session
            request.getSession().setAttribute("user", user);
            response.sendRedirect(request.getContextPath() + "/page/user/index.jsp");
        } else {
            request.setAttribute("titleMsg", "Woops！");
            request.setAttribute("textMsg", "The page is missing, please try again later");
            String url = request.getContextPath() + "/page/user/login.jsp";
            request.setAttribute("urlMsg", url);
            request.setAttribute("pageMsg", "Login");
            request.setAttribute("codeMsg", "404");
            request.getRequestDispatcher("/page/user/message.jsp").forward(request, response);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
