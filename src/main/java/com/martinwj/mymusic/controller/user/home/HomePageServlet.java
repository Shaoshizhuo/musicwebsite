package com.martinwj.mymusic.controller.user.home;

import com.martinwj.mymusic.entity.Comment;
import com.martinwj.mymusic.entity.Singer;
import com.martinwj.mymusic.entity.Song;
import com.martinwj.mymusic.entity.Type;
import com.martinwj.mymusic.service.CommentService;
import com.martinwj.mymusic.service.SingerService;
import com.martinwj.mymusic.service.SongService;
import com.martinwj.mymusic.service.TypeService;
import com.martinwj.mymusic.service.impl.CommentServiceImpl;
import com.martinwj.mymusic.service.impl.SingerServiceImpl;
import com.martinwj.mymusic.service.impl.SongServiceImpl;
import com.martinwj.mymusic.service.impl.TypeServiceImpl;
import org.apache.commons.collections.list.TypedList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @ClassName: HomePageServlet
 * @Description: TODO
 * @author: Shao Shizhuo
 * @createDate: 2023-1-16
 *
 */
@WebServlet("/homePageServlet")
public class HomePageServlet extends HttpServlet {

    private SongService songService = null;
    private SingerService singerService = null;
    private CommentService commentService = null;
    private TypeService typeService = null;

    @Override
    public void init() throws ServletException {
        songService = new SongServiceImpl();
        singerService = new SingerServiceImpl();
        commentService = new CommentServiceImpl();
        typeService = new TypeServiceImpl();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession();

        // Get 10 pieces of newly updated music
        List<Song> songList = songService.getNewMusic(10);

        // Store the map and get the corresponding singer information
        Map<Singer, Song> singerSongMap = new HashMap<>();
        for(Song song : songList) {
            Singer singer = singerService.getSingerBySingerId(song.getSingerId());
            singerSongMap.put(singer, song);
        }
        session.setAttribute("singerSongMap", singerSongMap);

        // The latest reply message
        List<Comment> comments = commentService.getNewComment(5);
        session.setAttribute("comments", comments);

        // A list of the most popular songs of late
        List<Type> typeList = typeService.getTypesBySongCount(8);
        List<Map<Singer, Song>> list = new ArrayList<>();
        for(int i = 0; i < typeList.size(); i++) {
            Map<Singer, Song> temp = new HashMap<>();
            // Query 8 pieces of data for even pages and 6 pieces of data for singular pages: for page layout
            int num = (i + 1) % 2 == 0 ? 8 : 6;
            for (Song song : songService.getSongByTypeWithRank(typeList.get(i), 0.36, 0.45, 0.19, num)) {
                temp.put(singerService.getSingerBySingerId(song.getSingerId()), song);
            }
            list.add(temp);
        }
        session.setAttribute("typeRankList", typeList);
        session.setAttribute("songRankList", list);

        response.sendRedirect(request.getContextPath() + "/page/user/index.jsp");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
