package com.martinwj.mymusic.controller.user.song.album;

import com.martinwj.mymusic.entity.Song;
import com.martinwj.mymusic.service.SongService;
import com.martinwj.mymusic.service.impl.SongServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;

/**
 * @ClassName: downLoadServlet
 * @Description: TODO
 * @author: Shao Shizhuo
 * @createDate: 2023-1-17
 *
 */
@WebServlet("/user/person/downLoadServlet")
public class DownLoadServlet extends HttpServlet {

    private SongService songService = null;


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //Set the response message encoding
        response.setCharacterEncoding("utf-8");
        //Obtain information about the downloaded file
        String idStr = request.getParameter("songId");
        System.out.println("SongID：" + idStr);
        int songId = Integer.parseInt(idStr);

        //Locate the file information from the database
        Song song = songService.getSongById(songId);

        //Gets the name of the file to download
        String absPath = request.getSession().getServletContext().getRealPath("");
        System.out.println(absPath);
        String filepath = absPath + "/" + song.getUrl();
        //Encodes the file name
//  		filename = new String(filename.trim().getBytes("iso8859-1"),"UTF-8");
        System.out.println("The path of the downloaded file is：" + filepath);

        File file = new File(filepath);
        System.out.println("Whether the file exists：" + file.exists());
        //If the file doesn't exist
        if (!file.exists()) {
            request.setAttribute("titleMsg", "Woops！");
            request.setAttribute("textMsg", "The resource you want to download no longer exists！");
            String url = request.getContextPath() + "/page/user/album-single.jsp";
            request.setAttribute("urlMsg", url);
            request.setAttribute("pageMsg", "Playlist");
            request.setAttribute("codeMsg", "");
            request.getRequestDispatcher("/page/user/message.jsp").forward(request, response);
            return;
        }
        //Change the download count
        System.out.println("Before Change：" + song.getDownloadCount());
        song.setDownloadCount(song.getDownloadCount() + 1);
        System.out.println("After Change：" + song.getDownloadCount());

        int flag = songService.updateSong(song);
        System.out.println("Whether the download change was successful：" + flag);
        if (flag > 0) {
//            String folder = "WebContent/download/";
            //Processing file name
            String realname = song.getName() + ".mp3";
            //Set the response header to control the browser to download the file
            response.setContentType("application/x-msdownload");
            response.setHeader("content-disposition", "attachment;filename=" + URLEncoder.encode(realname, "UTF-8"));
            //Read the file to be downloaded and save it to the file input stream
            FileInputStream in = new FileInputStream(filepath);
            //Create output stream
            OutputStream out = response.getOutputStream();
            //Create buffer
            byte[] buffer = new byte[1024];
            int len = 0;
            //The loop reads the contents of the input stream into the buffer
            while ((len = in.read(buffer)) > 0) {
                //Output buffer content to the browser to achieve file download
                out.write(buffer, 0, len);
            }
            in.close();
            out.close();
            System.out.print("File Download completed");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }


    @Override
    public void init() throws ServletException {
        songService = new SongServiceImpl();
    }
}
