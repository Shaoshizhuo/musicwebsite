package com.martinwj.mymusic.controller.user.song.album;

import com.martinwj.mymusic.entity.PageBean;
import com.martinwj.mymusic.entity.Song;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.List;

/**
 * @ClassName: delSongInPlayListServlet
 * @Description: TODO
 * @author: Shao Shizhuo
 * @createDate: 2023-1-17
 *
 */
@WebServlet("/delSongInPlayListServlet")
public class DelSongInPlayListServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String songId = request.getParameter("songId");
        String page = request.getParameter("page");
        System.out.println(page);
        int id= Integer.parseInt(songId);

        HttpSession session = request.getSession();
        LinkedHashSet<Song> songPlayList = (LinkedHashSet<Song>) session.getAttribute("songPlayList");

        System.out.println("Before Delete：" + songPlayList.size());
        // Removes the music from the playlist
        for(Song song : songPlayList){
            if(song.getSongId() == id) {
                songPlayList.remove(song);
                break;
            }
        }
        System.out.println("After Delete：" + songPlayList.size());
        session.setAttribute("songPlayList", songPlayList);

        // Redirect to the album-single page
        response.sendRedirect(request.getContextPath() + page);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
