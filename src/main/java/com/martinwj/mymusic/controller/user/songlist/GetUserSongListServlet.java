package com.martinwj.mymusic.controller.user.songlist;

import com.martinwj.mymusic.service.SongListService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @ClassName: getUserSongListServlet
 * @Description: TODO
 * @author: Shao Shizhuo
 * @createDate: 2023-1-23
 *
 */
@WebServlet("/user/person/getUserSongListServlet")
public class GetUserSongListServlet extends HttpServlet {

    private SongListService songListService = null;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // Redirect to Profile
        response.sendRedirect(request.getContextPath() + "/page/user/person/profile.jsp");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
