package com.martinwj.mymusic.controller.user.song.album;

import com.martinwj.mymusic.entity.PageBean;
import com.martinwj.mymusic.entity.Song;
import com.martinwj.mymusic.service.SongService;
import com.martinwj.mymusic.service.impl.SongServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
/**
 * @ClassName: addMusicToPlayListServlet
 * @Description: TODO
 * @author: Shao Shizhuo
 * @createDate: 2023-1-17
 *
 */
@WebServlet("/addMusicToPlayListServlet")
public class AddMusicToPlayListServlet extends HttpServlet {

    private SongService songService = null;

    @Override
    public void init() throws ServletException {
        songService = new SongServiceImpl();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        System.out.println("Add a song to the playlist");
        String songId = request.getParameter("songId");
        String page = request.getParameter("page");
        Song song = songService.getSongById(Integer.parseInt(songId));

        HttpSession session = request.getSession();
        LinkedHashSet<Song> songPlayList = (LinkedHashSet<Song>) session.getAttribute("songPlayList");

        if(songPlayList == null) {
            songPlayList = new LinkedHashSet<Song>();
        }

        // Determines whether the song already exists in the list
        boolean flag = true;
        for(Song s : songPlayList) {
            if(s.getSongId() == song.getSongId())
                flag = false;
        }
        // If flag is true, the song does not exist in the playlist.
        if(flag) {
            songPlayList.add(song);
        }

        // 增加播Increase the number of plays放数量
        songService.addPlayCountBySongId(song.getSongId());

        //
        session.setAttribute("songPlayList", songPlayList);

        // Redirect to the album-single page
        response.sendRedirect(request.getContextPath() + page);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
