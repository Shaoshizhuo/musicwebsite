package com.martinwj.mymusic.controller.user.song;

import com.martinwj.mymusic.service.SongService;
import com.martinwj.mymusic.service.impl.SongServiceImpl;
import com.martinwj.mymusic.util.ServletUtils;
import net.sf.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @ClassName: addSongPlayCountServlet
 * @Description: TODO
 * @author: Shao Shizhuo
 * @createDate: 2023-1-23
 *
 */
@WebServlet("/addSongPlayCountServlet")
public class AddSongPlayCountServlet extends HttpServlet {

    private SongService songService = null;

    @Override
    public void init() throws ServletException {
        songService = new SongServiceImpl();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String songId = request.getParameter("songId");
        System.out.println("Add the song ID as " + songId + " The number of play！");
        // Increase the number of times the song is played
        songService.addPlayCountBySongId(Integer.parseInt(songId));

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("status", "ok");
        ServletUtils.sendToPage(response, jsonObject.toString());
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
