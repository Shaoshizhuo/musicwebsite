package com.martinwj.mymusic.controller.user.singer;

import com.martinwj.mymusic.entity.Singer;
import com.martinwj.mymusic.service.SingerService;
import com.martinwj.mymusic.service.impl.SingerServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * @ClassName: GetAllSingerServlet
 * @Description: TODO
 * @author: Shao Shizhuo
 * @createDate: 2023-1-16
 *
 */
@WebServlet("/getAllSingerServlet")
public class GetAllSingerServlet extends HttpServlet {

    private SingerService singerService = null;

    @Override
    public void init() throws ServletException {
        singerService = new SingerServiceImpl();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Get all singer information
        List<Singer> allSingers = singerService.getAllSingers();
        System.out.println(allSingers);
        // Add to session
        HttpSession session = request.getSession();
        session.setAttribute("allSingers", allSingers);
        // Redirect to the album page
        response.sendRedirect(request.getContextPath() + "/page/user/albums.jsp");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
