package com.martinwj.mymusic.controller.user;

import com.martinwj.mymusic.entity.User;
import com.martinwj.mymusic.service.UserService;
import com.martinwj.mymusic.service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @ClassName: activeServlet
 * @Description: TODO
 * @author: Shao Shizhuo
 * @createDate: 2023-1-28
 *
 */
@WebServlet("/user/activeServlet")
public class ActiveServlet extends HttpServlet {

    private UserService userService = null;

    @Override
    public void init() throws ServletException {
        userService = new UserServiceImpl();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String codeParm = request.getParameter("code");
        String username = request.getParameter("username");
        String code = (String) request.getSession().getAttribute("code");
        User user = userService.getUserByUsername(username);
        if (user != null) {
            // Edit user active status
            if(userService.updateUserActive(username) > 0) {
                request.setAttribute("titleMsg", "Active Successfully！");
                request.setAttribute("textMsg", "Go to Login！");
                String url = request.getContextPath() + "/page/user/login.jsp";
                request.setAttribute("urlMsg", url);
                request.setAttribute("pageMsg", "Login");
                request.setAttribute("codeMsg", "");
            } else {
                request.setAttribute("titleMsg", "Woops！");
                request.setAttribute("textMsg", "The page you are looking for is missing!");
                String url = request.getContextPath() + "/page/user/register.jsp";
                request.setAttribute("urlMsg", url);
                request.setAttribute("pageMsg", "Register");
                request.setAttribute("codeMsg", "404");
            }
        }

//        if (user != null && user.getActive() == 0) {
//            if (code == null || code == "") {
//                if(userService.delUserByUsername(username) > 0){
//                    request.setAttribute("titleMsg", "Woops！");
//                    request.setAttribute("textMsg", "The page you are looking for is missing!");
//                    String url = request.getContextPath() + "/page/user/register.jsp";
//                    request.setAttribute("urlMsg", url);
//                    request.setAttribute("pageMsg", "Register");
//                    request.setAttribute("codeMsg", "404");
//                } else {
//                    request.setAttribute("titleMsg", "Active Unsuccessfully！");
//                    request.setAttribute("textMsg", "Too long, the user information is invalid, please re-register！");
//                    String url = request.getContextPath() + "/page/user/register.jsp";
//                    request.setAttribute("urlMsg", url);
//                    request.setAttribute("pageMsg", "Register");
//                    request.setAttribute("codeMsg", "");
//                }
//            }
//            else if(code.equals(codeParm)) {
//                if(userService.updateUserActive(username) > 0) {
//                    request.setAttribute("titleMsg", "Active Successfully！");
//                    request.setAttribute("textMsg", "Go to Login！");
//                    String url = request.getContextPath() + "/page/user/login.jsp";
//                    request.setAttribute("urlMsg", url);
//                    request.setAttribute("pageMsg", "Login");
//                    request.setAttribute("codeMsg", "");
//                } else {
//                    request.setAttribute("titleMsg", "Woops！");
//                    request.setAttribute("textMsg", "The page you are looking for is missing!");
//                    String url = request.getContextPath() + "/page/user/register.jsp";
//                    request.setAttribute("urlMsg", url);
//                    request.setAttribute("pageMsg", "Register");
//                    request.setAttribute("codeMsg", "404");
//                }
//            } else {
//                request.setAttribute("titleMsg", "Woops！");
//                request.setAttribute("textMsg", "The page you are looking for is missing!");
//                String url = request.getContextPath() + "/page/user/register.jsp";
//                request.setAttribute("urlMsg", url);
//                request.setAttribute("pageMsg", "Register");
//                request.setAttribute("codeMsg", "404");
//            }
//        } else {
//            request.setAttribute("titleMsg", "Woops！");
//            request.setAttribute("textMsg", "The user information does not exist, please re-register");
//            String url = request.getContextPath() + "/page/user/register.jsp";
//            request.setAttribute("urlMsg", url);
//            request.setAttribute("pageMsg", "Rsgister");
//            request.setAttribute("codeMsg", "");
//        }
        request.getRequestDispatcher("/page/user/message.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
