package com.martinwj.mymusic.controller.admin.comment;

import com.martinwj.mymusic.entity.Comment;
import com.martinwj.mymusic.entity.PageBean;
import com.martinwj.mymusic.entity.Song;
import com.martinwj.mymusic.service.CommentService;
import com.martinwj.mymusic.service.impl.CommentServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Arrays;
import java.util.Map;

/**
 * @ClassName: FindCommentByPageServlet
 * @Description: TODO
 * @author: Shao Shizhuo
 * @createDate: 2023-1-12
 *
 */
@WebServlet("/admin/findCommentByPageServlet")
public class FindCommentByPageServlet extends HttpServlet {

    private CommentService commentService = null;

    @Override
    public void init() throws ServletException {
        commentService = new CommentServiceImpl();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
// Obtain parameters
        String currentPage = request.getParameter("currentPage");// Current Page
        String rows = request.getParameter("rows");// Number of items per page

        if(currentPage == null || "".equals(currentPage)){
            currentPage = "1";
        }

        if(rows == null || "".equals(rows)){
            rows = "6";
        }

        //Obtain Condition Query parameters
        Map<String, String[]> condition = request.getParameterMap();

        // Modify parameters


        //2.Call service query
        PageBean<Comment> pb = commentService.findCommentsByPage(currentPage, rows, condition);
        System.out.println(pb);

        for(Comment comment : pb.getList()) {
            comment.setContext(comment.getContext().replace("\n", "<br>"));
        }

        //3.Store the PageBean in session
        HttpSession session = request.getSession();
        for(String s : condition.keySet()){
            System.out.println(s + " : " + Arrays.toString(condition.get(s)));
            session.setAttribute("commentKeyName", condition.get(s)[0]);
        }
        session.setAttribute("commentPb", pb);
        session.setAttribute("commentCondition", condition);// The query conditions are saved to the request
        request.getRequestDispatcher("/page/manager/comment.jsp").forward(request, response);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
