package com.martinwj.mymusic.controller.admin.user;

import com.martinwj.mymusic.entity.PageBean;
import com.martinwj.mymusic.entity.Song;
import com.martinwj.mymusic.entity.User;
import com.martinwj.mymusic.service.UserService;
import com.martinwj.mymusic.service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Arrays;
import java.util.Map;

/**
 * @ClassName: ShowUsersByPageServlet
 * @Description: TODO
 * @author: Shao Shizhuo
 * @createDate: 2023-1-23
 *
 */
@WebServlet("/admin/showUsersByPageServlet")
public class ShowUsersByPageServlet extends HttpServlet {

    private UserService userService = null;

    @Override
    public void init() throws ServletException {
        userService = new UserServiceImpl();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // Get parameters
        String currentPage = request.getParameter("currentPage");// Current Page
        String rows = request.getParameter("rows");// Number of items displayed per page

        if(currentPage == null || "".equals(currentPage)){
            currentPage = "1";
        }

        if(rows == null || "".equals(rows)){
            rows = "6";
        }

        //Get conditional query parameters
        Map<String, String[]> condition = request.getParameterMap();

        System.out.println(condition);

        //2.call service query
        PageBean<User> pb = userService.findUsersByPage(currentPage, rows, condition);
        System.out.println(pb);

        //3.Store PageBean in session
        HttpSession session = request.getSession();
        for(String s : condition.keySet()){
            System.out.println(s + " : " + Arrays.toString(condition.get(s)));
            session.setAttribute("userKeyName", condition.get(s)[0]);
        }
        session.setAttribute("userPb", pb);
        session.setAttribute("userCondition", condition);// Store query conditions in request
        request.getRequestDispatcher("/page/manager/user.jsp").forward(request, response);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
