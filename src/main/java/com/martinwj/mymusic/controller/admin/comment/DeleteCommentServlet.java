package com.martinwj.mymusic.controller.admin.comment;

import com.martinwj.mymusic.service.CommentService;
import com.martinwj.mymusic.service.impl.CommentServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * @ClassName: DeleteCommentServlet
 * @Description: TODO
 * @author: Shao Shizhuo
 * @createDate: 2023-1-12
 *
 */
@WebServlet("/admin/deleteCommentServlet")
public class DeleteCommentServlet extends HttpServlet {

    private CommentService commentService = null;

    @Override
    public void init() throws ServletException {
        commentService = new CommentServiceImpl();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //1.Get all ids
        String[] ids = request.getParameterValues("commentId");
        System.out.println("The comment ID that was deleted：" + Arrays.toString(ids));

        // 2.Call service delete
        List<Integer> list = commentService.delCommentByCommentIds(ids);

        //3.Jump queries all servlets
        response.sendRedirect(request.getContextPath()+"/admin/findCommentByPageServlet");

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
