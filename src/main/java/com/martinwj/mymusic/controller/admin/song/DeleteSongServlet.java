package com.martinwj.mymusic.controller.admin.song;

import com.martinwj.mymusic.dao.SongDao;
import com.martinwj.mymusic.dao.impl.SongDaoImpl;
import com.martinwj.mymusic.entity.Song;
import com.martinwj.mymusic.service.CommentService;
import com.martinwj.mymusic.service.SongService;
import com.martinwj.mymusic.service.TypeService;
import com.martinwj.mymusic.service.impl.CommentServiceImpl;
import com.martinwj.mymusic.service.impl.SongServiceImpl;
import com.martinwj.mymusic.service.impl.TypeServiceImpl;
import com.martinwj.mymusic.util.ServletUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * @ClassName: DeleteSongServlet
 * @Description: TODO
 * @author: Shao Shizhuo
 * @createDate: 2023-1-22
 *
 */
@WebServlet("/admin/deleteSongServlet")
public class DeleteSongServlet extends HttpServlet {

    private SongService songService = null;
    private TypeService typeService = null;
    private CommentService commentService = null;

    @Override
    public void init() throws ServletException {
        songService = new SongServiceImpl();
        typeService = new TypeServiceImpl();
        commentService = new CommentServiceImpl();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //1.get all ids
        String[] ids = request.getParameterValues("songId");
        System.out.println(Arrays.toString(ids));

        //2. While deleting, modify the number of songs in this category
        ServletUtils.deleteSong(ids);

        //3.Jump to query all Servlets
        response.sendRedirect(request.getContextPath()+"/admin/findSongByPageServlet");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
