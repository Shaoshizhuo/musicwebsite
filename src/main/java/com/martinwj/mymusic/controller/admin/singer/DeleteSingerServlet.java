package com.martinwj.mymusic.controller.admin.singer;

import com.martinwj.mymusic.entity.Song;
import com.martinwj.mymusic.service.SingerService;
import com.martinwj.mymusic.service.SongService;
import com.martinwj.mymusic.service.impl.SingerServiceImpl;
import com.martinwj.mymusic.service.impl.SongServiceImpl;
import com.martinwj.mymusic.util.ServletUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * @ClassName: DeleteSingerServlet
 * @Description: TODO
 * @author: Shao Shizhuo
 * @createDate: 2023-1-20
 *
 */
@WebServlet("/admin/deleteSingerServlet")
public class DeleteSingerServlet extends HttpServlet {

    private SingerService singerService = null;
    private SongService songService = null;

    @Override
    public void init() throws ServletException {
        singerService = new SingerServiceImpl();
        songService = new SongServiceImpl();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //1.get all ids
        String[] ids = request.getParameterValues("singerId");
        System.out.println(Arrays.toString(ids));

        for(String id : ids) {
            // To delete a song is to delete its song information first
            List<Song> list = songService.getSongBySingerId(Integer.parseInt(id));
            for (Song song : list) {
                ServletUtils.deleteSong(song.getSongId());
            }
        }

        // 2.Call service to delete singer information
        List<Integer> list = singerService.deleteSingerBySingerId(ids);

        //3.Jump to query all Servlets
        response.sendRedirect(request.getContextPath()+"/admin/findSingerByPageServlet");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
