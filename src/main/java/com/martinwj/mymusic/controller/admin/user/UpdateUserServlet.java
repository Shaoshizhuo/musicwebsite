package com.martinwj.mymusic.controller.admin.user;

import com.martinwj.mymusic.entity.User;
import com.martinwj.mymusic.service.UserService;
import com.martinwj.mymusic.service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @ClassName: UpdateUserServlet
 * @Description: TODO
 * @author: Shao Shizhuo
 * @createDate: 2023-1-23
 *
 */
@WebServlet("/admin/updateUserServlet")
public class UpdateUserServlet extends HttpServlet {

    private UserService userService = null;

    @Override
    public void init() throws ServletException {
        userService = new UserServiceImpl();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String userId = request.getParameter("userId");
        String oldPassword = request.getParameter("old_password_" + userId);
        String newPassword = request.getParameter("update_password_" + userId);

        System.out.println("Edit Password：" + oldPassword + "  :  " + newPassword);
        if(!newPassword.equals(oldPassword)) {
            // Call service to change password
            int flag = userService.updateUserPassword(Integer.parseInt(userId), newPassword);
        }

        // Jump to query all Servlets
        response.sendRedirect(request.getContextPath()+"/admin/showUsersByPageServlet");

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
