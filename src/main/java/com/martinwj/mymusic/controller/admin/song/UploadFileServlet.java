package com.martinwj.mymusic.controller.admin.song;

import com.martinwj.mymusic.entity.*;
import com.martinwj.mymusic.service.SingerService;
import com.martinwj.mymusic.service.SongService;
import com.martinwj.mymusic.service.TypeService;
import com.martinwj.mymusic.service.impl.SingerServiceImpl;
import com.martinwj.mymusic.service.impl.SongServiceImpl;
import com.martinwj.mymusic.service.impl.TypeServiceImpl;
import com.martinwj.mymusic.util.*;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadBase;
import org.apache.commons.fileupload.ProgressListener;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * @ClassName: UploadFileServlet
 * @Description: TODO
 * @author: Shao Shizhuo
 * @createDate: 2023-1-22
 *
 */
@WebServlet("/admin/uploadFileServlet")
public class UploadFileServlet extends HttpServlet {
    private SingerService singerService = null;
    private SongService songService = null;
    private TypeService typeService = null;

    @Override
    public void init() throws ServletException {
        singerService = new SingerServiceImpl();
        songService = new SongServiceImpl();
        typeService = new TypeServiceImpl();
    }

    @SuppressWarnings("unchecked")
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=utf-8");
        BigDecimal price = null;

        Song song = new Song();

        HttpSession session = request.getSession();
        request.setAttribute("page", "music");
        String path_save = "/other/upload/";
        String path_temp = "/other/temp/";

        String realSavePath = null;
        String filename = null;
        String saveFilename = null;
        String savePath = PathUtils.getProjectURL() + "" + path_save;
        //Temporary file storage directory generated during upload
//        String tempPath = new File("").getCanonicalPath() + "\\web" + path_temp;
        String tempPath = PathUtils.getProjectURL() + "" + path_temp;
        System.out.println("savePath: " + savePath);
        System.out.println("tempPath: " + tempPath);

        File tmpFile = new File(tempPath);
        if (!tmpFile.exists()) {
            //Create a temporary directory
            tmpFile.mkdir();
        }

        //notification
        String message = "";
        String singerName = "";
        try{
            //Using the Apache file upload component to handle file uploading steps：
            //1、Create a DiskFileItemFactory factory
            DiskFileItemFactory factory = new DiskFileItemFactory();
            //Set the buffer size of the factory.
            // When the size of the uploaded file exceeds the buffer size, a temporary file will be generated and stored in the specified temporary directory.
            factory.setSizeThreshold(1024*100);//Set the size of the buffer to 100KB, if not specified, the default size of the buffer is 10KB
            //Set the save directory for temporary files generated during upload
            factory.setRepository(tmpFile);
            //2、Create a file upload parser
            ServletFileUpload upload = new ServletFileUpload(factory);
            //Monitor file upload progress
            upload.setProgressListener(new ProgressListener(){
                public void update(long pBytesRead, long pContentLength, int arg2) {
                    System.out.println("The file size is：" + pContentLength + ",currently processed：" + pBytesRead);
                }
            });
            //Solve the garbled characters in the uploaded file name
            upload.setHeaderEncoding("UTF-8");
            //3、Determine whether the submitted data is the data of the upload form
            if(!ServletFileUpload.isMultipartContent(request)){
                //Get data the traditional way
                System.out.println("Get data the traditional way！");
                request.setAttribute("message", "Server is busy！！！");
                request.getRequestDispatcher("/page/manager/message.jsp").forward(request, response);

                return;
            }

            ////Set the maximum size of uploading a single file, which is currently set to 1024*1024*100 bytes, which is 100MB
            upload.setFileSizeMax(1024*1024*20);
            //Set the maximum value of the total uploaded files, the maximum value = the sum of the maximum size of multiple files uploaded at the same time,
            // currently set to 10MB
            upload.setSizeMax(1024*1024*20);
            //4、Use the ServletFileUpload parser to parse the uploaded data, and the parsed result returns a List<FileItem> collection,
            // and each FileItem corresponds to an input item of a Form
            List<FileItem> list = upload.parseRequest(request);
            FileItem fileItem = null;

            for(FileItem item : list) {
                //If the data encapsulated in fileitem is the data of ordinary input items
                if (item.isFormField()) {
                    String name = item.getFieldName();
                    //Solve the problem of garbled characters in the data of ordinary input items
                    String value = item.getString("UTF-8");
                    //value = new String(value.getBytes("iso8859-1"),"UTF-8");
                    System.out.println(name + "=" + value);
                    if ("singerName".equals(name)) {
                        // Get singer ID
                        Singer singer = singerService.getSingerBySingerName(value);
                        System.out.println(singer);
                        if (singer == null) {
                            request.setAttribute("message", "There is no information about the singer！！！");
                            request.getRequestDispatcher("/page/manager/message.jsp").forward(request, response);
                            return;
                        } else {
                            song.setSingerId(singer.getSingerId());
                            song.setSingerName(value);
                            singerName = singer.getName();
                            System.out.println("Singer Name：" + singerName);
                        }
                    } else if ("songName".equals(name)) {
                        System.out.println(name);
                        song.setName(value);
                        if(songService.getSongByName(value) != null){
                            request.setAttribute("message", "Song already exists！！！");
                            request.getRequestDispatcher("/page/manager/message.jsp").forward(request, response);
                            return;
                        }
                    } else if ("language".equals(name)) {
                        System.out.println(name);
                        song.setLanguage(value);
                    } else if ("lyric".equals(name)) {
                        System.out.println(name);
                        song.setLyric(value);
                    } else if ("songType".equals(name)) {
                        System.out.println(name);
                        System.out.println(Arrays.toString(value.split("\n")));
                        song.setType(value);
                    }
                } else {
                    fileItem = item;
                }
            }
            //If the package in fileitem is uploaded file
            savePath = savePath + singerName;
            System.out.println("savePath: " + savePath);
            File savefile = new File(savePath);
            if (!savefile.exists()) {
                //Create a temporary directory
                savefile.mkdir();
            }

            //get uploaded file name
            filename = fileItem.getName();
            System.out.println("Uploaded file name："+filename);
            if(filename==null || filename.trim().equals("")){
                request.setAttribute("message", "Uploaded file name is empty！！！");
                request.getRequestDispatcher("/page/manager/message.jsp").forward(request, response);
                return;
            }
            String filename_1 = filename.substring(filename.lastIndexOf("\\")+1);
            //Get the extension of the uploaded file
            String fileExtName = filename_1.substring(filename_1.lastIndexOf(".")+1);
            //If you need to limit the uploaded file type, you can use the file extension to determine whether the uploaded file type is legal
            System.out.println("The extension of the uploaded file is："+fileExtName);
            //Get the input stream of the uploaded file in item
            InputStream in = fileItem.getInputStream();
            //get the name of the saved file
//                    saveFilename = makeFileName(filename);
            saveFilename = song.getName() + "." + fileExtName;
            //Get the directory where the file is saved
//                    realSavePath = makePath(saveFilename, savePath);
            realSavePath = savePath + "/" + filename;
            //Create a file output stream
            System.out.println("File output path：" + realSavePath);
            FileOutputStream out = new FileOutputStream(realSavePath);
            //create a buffer
            byte[] buffer = new byte[1024];
            //The flag to determine whether the data in the input stream has been read
            int len = 0;
            //The loop reads the input stream into the buffer, (len=in.read(buffer))>0 means that there is still data in
            while((len=in.read(buffer))>0){
                out.write(buffer, 0, len);
            }
            //close input stream
            in.close();
            //close output stream
            out.close();
            //Delete temporary files generated while processing file uploads
            //item.delete();
            message = "File uploaded successfully！";
            System.out.println("File uploaded successfully！");

        }catch (FileUploadBase.FileSizeLimitExceededException e) {
            e.printStackTrace();
            request.setAttribute("message", "A single file exceeds the maximum！！！");
            request.getRequestDispatcher("/page/manager/message.jsp").forward(request, response);
            return;
        }catch (FileUploadBase.SizeLimitExceededException e) {
            e.printStackTrace();
            request.setAttribute("message", "The total size of uploaded files exceeds the maximum limit！！！");
            request.getRequestDispatcher("/page/manager/message.jsp").forward(request, response);
            return;
        }catch (Exception e) {
            message= "File upload failed！";
            System.out.println("File upload failed！");
            e.printStackTrace();
        }

        addSong(request, song, realSavePath);
        System.out.println("message : " + request.getAttribute("message"));

        request.getRequestDispatcher("/page/manager/message.jsp").forward(request, response);
    }

    private String makeFileName(String filename){
        //In order to prevent the occurrence of file overwriting, a unique file name should be generated for the uploaded file
        return UUID.randomUUID().toString() + "_" + filename;
    }


    private String makePath(String filename,String savePath){
        int hashcode = filename.hashCode();
        int dir1 = hashcode&0xf;  //0--15
        int dir2 = (hashcode&0xf0)>>4;  //0-15
        String dir = savePath + "\\" + dir1 + "\\" + dir2;  //upload\2\3  upload\3\5
        File file = new File(dir);
        if(!file.exists()){
            file.mkdirs();
        }
        return dir;
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        doGet(request, response);
    }

    public void addSong(HttpServletRequest request, Song song, String realSavePath){
        System.out.println("Start adding song files");

        String url = realSavePath.substring(realSavePath.indexOf("other"));
        System.out.println(url);
        // encapsulated data
        song.setSongId(null);
        song.setCdId(null);
        song.setPlayCount(0);
        song.setDownloadCount(0);
        song.setCollectionCount(0);
        Date date = new Date();
        String dateStr = DateUtils.getDateString(date);
        song.setPublicDate(dateStr);
        song.setUpdateDate(dateStr);
        song.setUrl(url);
        song.setTime(Tools.getMP3Timer(realSavePath));

        // Set upload user
        Admin admin = (Admin) request.getSession().getAttribute("admin");
        User user = (User) request.getSession().getAttribute("user");
        if(admin != null) {
            song.setUploader(admin.getAdminName());
        } else if(user != null) {
            song.setUploader(user.getName());
        }
        System.out.println(song.getTime());
        int flag = songService.addSong(song);
        System.out.println(flag);
        if(flag > 0) {
            // The song is added successfully, modify the number of songs in this song category
            for(String type : song.getType().split(",")){
                if(typeService.getTypeByName(type) != null) {
                    typeService.updateTypeByName(type, true);
                } else {
                    typeService.addType(new Type(null, type, 1));
                }
            }
            request.setAttribute("message", "Song added successfully！");
        } else {
            request.setAttribute("message", "Song added failed！");
        }
    }

    public String mkFileName(String fileName){
        return UUID.randomUUID().toString()+"_"+fileName;
    }


}
