package com.martinwj.mymusic.controller.admin.song;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.martinwj.mymusic.entity.PageBean;
import com.martinwj.mymusic.entity.Song;
import com.martinwj.mymusic.service.SongService;
import com.martinwj.mymusic.service.impl.SongServiceImpl;
import com.martinwj.mymusic.util.ServletUtils;
import net.sf.json.JSON;
import net.sf.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Arrays;
import java.util.Map;

/**
 * @ClassName: FindSongByPageServlet
 * @Description: TODO
 * @author: Shao Shizhuo
 * @createDate: 2023-1-22
 *
 */
@WebServlet("/admin/findSongByPageServlet")
public class FindSongByPageServlet extends HttpServlet {

    private SongService songService = null;

    @Override
    public void init() throws ServletException {
        songService = new SongServiceImpl();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        // Get parameters
        String currentPage = request.getParameter("currentPage");// Current Page
        String rows = request.getParameter("rows");// Number of items displayed per page

        if(currentPage == null || "".equals(currentPage)){
            currentPage = "1";
        }

        if(rows == null || "".equals(rows)){
            rows = "6";
        }

        //1.Get conditional query parameters
        Map<String, String[]> condition = request.getParameterMap();

        //2.call service query
        PageBean<Song> pb = songService.findSongsByPage(currentPage, rows, condition);
        System.out.println(pb);

        //3.Store PageBean in session
        HttpSession session = request.getSession();
        for(String s : condition.keySet()){
            System.out.println(s + " : " + Arrays.toString(condition.get(s)));
            session.setAttribute("keyName", condition.get(s)[0]);
        }
        session.setAttribute("pb", pb);
        session.setAttribute("condition", condition);// Store query conditions in request
        request.getRequestDispatcher("/page/manager/music.jsp").forward(request, response);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
