package com.martinwj.mymusic.controller.admin.singer;

import com.martinwj.mymusic.entity.Singer;
import com.martinwj.mymusic.service.SingerService;
import com.martinwj.mymusic.service.impl.SingerServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @ClassName: UpdateSingerServlet
 * @Description: TODO
 * @author: Shao Shizhuo
 * @createDate: 2023-1-20
 *
 */
@WebServlet("/admin/updateSingerServlet")
public class UpdateSingerServlet extends HttpServlet {

    private SingerService singerService = null;

    @Override
    public void init() throws ServletException {
        singerService = new SingerServiceImpl();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // get modification form
        String singerId = request.getParameter("singerId");
        String accessCount = request.getParameter("update_accessCount_" + singerId);
        String collectionCount = request.getParameter("update_collectionCount_" + singerId);
        String location = request.getParameter("update_singerLocation_" + singerId);
        String introduction = request.getParameter("updateIntroduction_" + singerId);

        // encapsulated data
        Singer singer = new Singer();
        singer.setSingerId(Integer.parseInt(singerId));
        singer.setAccessCount(Integer.parseInt(accessCount));
        singer.setCollectionCount(Integer.parseInt(collectionCount));
        singer.setLocation(location);
        singer.setIntroduction(introduction);
        System.out.println(singer);

        // Call the service to perform the modification operation
        int flag = singerService.updateSinger(singer);

        // Jump to query all Servlets
        response.sendRedirect(request.getContextPath()+"/admin/findSingerByPageServlet");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
