package com.martinwj.mymusic.controller.admin.comment;

import com.martinwj.mymusic.service.CommentService;
import com.martinwj.mymusic.service.impl.CommentServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @ClassName: UpdateCommentServlet
 * @Description: TODO
 * @author: Shao Shizhuo
 * @createDate: 2023-1-12
 *
 */
@WebServlet("/admin/updateCommentServlet")
public class UpdateCommentServlet extends HttpServlet {

    private CommentService commentService = null;

    @Override
    public void init() throws ServletException {
        commentService = new CommentServiceImpl();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String commentId = request.getParameter("commentId");
        String newCommentContext = request.getParameter("updateContext_" + commentId);

        System.out.println("Edit comment content：" + newCommentContext);
        // Call service to modify the comment content
        int flag = commentService.updateCommentContext(Integer.parseInt(commentId), newCommentContext);

        // Jump queries all servlets
        response.sendRedirect(request.getContextPath()+"/admin/findCommentByPageServlet");

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}