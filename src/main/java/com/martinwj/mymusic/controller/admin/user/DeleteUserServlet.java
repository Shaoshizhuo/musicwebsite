package com.martinwj.mymusic.controller.admin.user;

import com.martinwj.mymusic.entity.Song;
import com.martinwj.mymusic.service.UserService;
import com.martinwj.mymusic.service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * @ClassName: DeleteUserServlet
 * @Description: TODO
 * @author: Shao Shizhuo
 * @createDate: 2023-1-23
 *
 */
@WebServlet("/admin/deleteUserServlet")
public class DeleteUserServlet extends HttpServlet {

    private UserService userService = null;

    @Override
    public void init() throws ServletException {
        userService = new UserServiceImpl();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //1.get all ids
        String[] ids = request.getParameterValues("userId");
        System.out.println(Arrays.toString(ids));

        // 2.Call service to delete
        List<Integer> list = userService.delUserByUserIds(ids);

        //3.Jump to query all Servlets
        response.sendRedirect(request.getContextPath()+"/admin/showUsersByPageServlet");

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
