package com.martinwj.mymusic.controller.admin;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @ClassName: AdminLoginoutServlet
 * @Description: TODO
 * @author: Shao Shizhuo
 * @createDate: 2023-1-10
 *
 */
@WebServlet("/admin/loginoutServlet")
public class AdminLoginoutServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        HttpSession session = request.getSession();
        session.setAttribute("admin", null);

        // Redirect to the login page
        response.sendRedirect(request.getContextPath() + "/page/manager/login.jsp");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
