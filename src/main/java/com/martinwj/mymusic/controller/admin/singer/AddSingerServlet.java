package com.martinwj.mymusic.controller.admin.singer;

import com.martinwj.mymusic.entity.Singer;
import com.martinwj.mymusic.service.SingerService;
import com.martinwj.mymusic.service.SongService;
import com.martinwj.mymusic.service.impl.SingerServiceImpl;
import com.martinwj.mymusic.service.impl.SongServiceImpl;
import com.martinwj.mymusic.util.PathUtils;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadBase;
import org.apache.commons.fileupload.ProgressListener;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.UUID;

/**
 * @ClassName: AddSingerServlet
 * @Description: TODO
 * @author: Shao Shizhuo
 * @createDate: 2023-1-20
 *
 */
@WebServlet("/admin/addSingerServlet")
public class AddSingerServlet extends HttpServlet {
    private SingerService singerService = null;
    private SongService songService = null;

    @Override
    public void init() throws ServletException {
        singerService = new SingerServiceImpl();
        songService = new SongServiceImpl();
    }

    @Override
    @SuppressWarnings("unchecked")
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub

        Singer singer = new Singer();

        HttpSession session = request.getSession();
        request.setAttribute("page", "singer");
        String path_save = "/other/img/";
        String path_temp = "/other/temp/";

        String realSavePath = null;
        String filename = null;
        String saveFilename = null;
        //After obtaining the directory for storing uploaded files, store the uploaded files in the WEB-INF directory.
        // External users are not allowed to access the uploaded files
//        String savePath = new File("").getCanonicalPath() + "\\web" + path_save;
        String savePath = PathUtils.getProjectURL() + "/web" + path_save;
        //Directory for saving temporary files generated during upload
//        String tempPath = new File("").getCanonicalPath() + "\\web" + path_temp;
        String tempPath = PathUtils.getProjectURL() + "/web" + path_temp;
        System.out.println("savePath: " + savePath);
        System.out.println("tempPath: " + tempPath);

        File tmpFile = new File(tempPath);
        if (!tmpFile.exists()) {
            //Create a temporary directory
            tmpFile.mkdir();
        }

        //Message prompt
        String message = "";
        String singerName = "";
        try{
            //Using the Apache file upload component to handle file uploading steps：
            //1、Create a DiskFileItemFactory factory
            DiskFileItemFactory factory = new DiskFileItemFactory();
            //Set the buffer size of the factory.
            // When the size of the uploaded file exceeds the buffer size, a temporary file will be generated and stored in the specified temporary directory.
            factory.setSizeThreshold(1024*100);//Set the size of the buffer to 100KB, if not specified, the default size of the buffer is 10KB
            //Set the save directory for temporary files generated during upload
            factory.setRepository(tmpFile);
            //2、Create a file upload parser
            ServletFileUpload upload = new ServletFileUpload(factory);
            //Monitor file upload progress
            upload.setProgressListener(new ProgressListener(){
                @Override
                public void update(long pBytesRead, long pContentLength, int arg2) {
                    System.out.println("The file size is：" + pContentLength + ",currently processed：" + pBytesRead);
                }
            });
            //Solve the garbled characters in the uploaded file name
            upload.setHeaderEncoding("UTF-8");
            //3、Determine whether the submitted data is the data of the upload form
            if(!ServletFileUpload.isMultipartContent(request)){
                //Get data the traditional way
                System.out.println("Get data the traditional way！");
                request.setAttribute("message", "Server is busy！！！");
                request.getRequestDispatcher("/page/manager/message.jsp").forward(request, response);

                return;
            }

            //Set the maximum size of uploading a single file, which is currently set to 1024*1024*100 bytes, which is 100MB
            upload.setFileSizeMax(1024*1024*5);
            //Set the maximum value of the total uploaded files, the maximum value = the sum of the maximum size of multiple files uploaded at the same time,
            // currently set to 10MB
            upload.setSizeMax(1024*1024*10);
            //4、Use the ServletFileUpload parser to parse the uploaded data, and the parsed result returns a List<FileItem> collection,
            // and each FileItem corresponds to an input item of a Form
            List<FileItem> list = upload.parseRequest(request);
            FileItem fileItem = null;

            for(FileItem item : list) {
                //If the data encapsulated in fileitem is the data of ordinary input items
                if (item.isFormField()) {
                    String name = item.getFieldName();
                    //Solve the problem of garbled characters in the data of ordinary input items
                    String value = item.getString("UTF-8");
                    //value = new String(value.getBytes("iso8859-1"),"UTF-8");
                    System.out.println(name + "=" + value);
                    if ("name".equals(name)) {
                        // Get singer ID
                        singer.setName(value);
                        System.out.println(singer);
                    } else if ("sex".equals(name)) {
                        singer.setSex(Integer.parseInt(value));
                        System.out.println("Gender：" + value);
                    } else if ("birth".equals(name)) {
                        singer.setBirth(value);
                    } else if ("location".equals(name)) {
                        singer.setLocation(value);
                    } else if ("introduction".equals(name)) {
                        System.out.println(name);
                        singer.setIntroduction(value);
                    } else if ("debutDate".equals(name)) {
                        singer.setDebutDate(value);
                    } else if ("accessCount".equals(name)) {
                        singer.setAccessCount(Integer.parseInt(value));
                    } else if ("englishName".equals(name)) {
                        singer.setEnglishName(value);
                    }
                } else {
                    fileItem = item;
                }
            }
            //If the package in fileitem is uploaded file
            savePath = savePath + singerName;
            System.out.println("savePath: " + savePath);
            File savefile = new File(savePath);
            if (!savefile.exists()) {
                //Create a temporary directory
                savefile.mkdir();
            }

            //get uploaded file name
            filename = fileItem.getName();
            System.out.println("Uploaded file name："+filename);
            if(filename==null || filename.trim().equals("")){
                // The file that allows long transfer is empty, so jump out directly here
                realSavePath = null;
            }else {
                String filename_1 = filename.substring(filename.lastIndexOf("\\") + 1);
                //Get the extension of the uploaded file
                String fileExtName = filename_1.substring(filename_1.lastIndexOf(".") + 1);
                //If you need to limit the uploaded file type, you can use the file extension to determine whether the uploaded file type is legal
                System.out.println("The extension of the uploaded file is：" + fileExtName);
                //Get the input stream of the uploaded file in item
                InputStream in = fileItem.getInputStream();
                //get the name of the saved file
//                    saveFilename = makeFileName(filename);
                saveFilename = singer.getName() + "." + fileExtName;
                //Get the directory where the file is saved
//                    realSavePath = makePath(saveFilename, savePath);
                realSavePath = savePath + "/" + filename;
                //Create a file output stream
                System.out.println("File output path：" + realSavePath);
                FileOutputStream out = new FileOutputStream(realSavePath);
                //create a buffer
                byte[] buffer = new byte[1024];
                //The flag to determine whether the data in the input stream has been read
                int len = 0;
                //The loop reads the input stream into the buffer, (len=in.read(buffer))>0 means that there is still data in
                while ((len = in.read(buffer)) > 0) {
                    out.write(buffer, 0, len);
                }
                //close input stream
                in.close();
                //close output stream
                out.close();
                //Delete temporary files generated while processing file uploads
                //item.delete();
                message = "File uploaded successfully！";
                System.out.println("File uploaded successfully！");
            }
        }catch (FileUploadBase.FileSizeLimitExceededException e) {
            e.printStackTrace();
            request.setAttribute("message", "A single file exceeds the maximum！！！");
            request.getRequestDispatcher("/page/manager/message.jsp").forward(request, response);
            return;
        }catch (FileUploadBase.SizeLimitExceededException e) {
            e.printStackTrace();
            request.setAttribute("message", "The total size of uploaded files exceeds the maximum limit！！！");
            request.getRequestDispatcher("/page/manager/message.jsp").forward(request, response);
            return;
        }catch (Exception e) {
            message= "File upload failed！";
            System.out.println("File upload failed！");
            e.printStackTrace();
        }

        if(realSavePath == null){
            addSinger(request, singer);
        }else {
            addSinger(request, singer, realSavePath);
        }
        System.out.println("message : " + request.getAttribute("message"));

        request.getRequestDispatcher("/page/manager/message.jsp").forward(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        doGet(request, response);
    }

    public void addSinger(HttpServletRequest request, Singer singer, String realSavePath){

        String url = realSavePath.substring(realSavePath.indexOf("other"));
        System.out.println(url);
        // encapsulated data
        singer.setPic(url);
       addSinger(request, singer);
    }

    public void addSinger(HttpServletRequest request, Singer singer){
        // encapsulated data
        singer.setCollectionCount(0);
        int flag = singerService.addSinger(singer);
        System.out.println(flag);
        if(flag > 0) {
            request.setAttribute("message", "Singer added successfully！");
        } else {
            request.setAttribute("message", "Singer added failed！");
        }
    }


    public String mkFileName(String fileName){
        return UUID.randomUUID().toString()+"_"+fileName;
    }


}
