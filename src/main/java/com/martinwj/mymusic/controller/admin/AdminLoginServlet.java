package com.martinwj.mymusic.controller.admin;

import com.martinwj.mymusic.dao.AdminDao;
import com.martinwj.mymusic.dao.impl.AdminDaoImpl;
import com.martinwj.mymusic.entity.Admin;
import com.martinwj.mymusic.service.AdminService;
import com.martinwj.mymusic.service.impl.AdminServiceImpl;
import com.martinwj.mymusic.util.ServletUtils;
import net.sf.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import java.util.Objects;

/**
 * @ClassName: AdminLoginServlet
 * @Description: TODO
 * @author: Shao Shizhuo
 * @createDate: 2023-1-10
 *
 */
@WebServlet("/admin/loginServlet")
public class AdminLoginServlet extends HttpServlet {

    private AdminService adminService = null;

    @Override
    public void init() throws ServletException {
        adminService = new AdminServiceImpl();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");

        // Get the data from the front end
        String login = req.getParameter("login");
        String pwd = req.getParameter("pwd");
        String inputCode = req.getParameter("code");
        System.out.println(inputCode);

        // Obtain the verification code stored in the session
        HttpSession session = req.getSession();
        String code = (String) session.getAttribute("code");
        System.out.println(code);

        // Define the return data in json format
        JSONObject jsonObject = new JSONObject();
        // Check whether the verification code is correct
        if(code != null && code.equalsIgnoreCase(inputCode)){
            // Check whether the user name exists
            if(adminService.usernameIsExit(login) != null){
                Admin admin = adminService.getAdmin(login, pwd);
                if(admin != null){
                    jsonObject.put("Status", "ok");
                    jsonObject.put("Text", "Login Successfully<br /><br />Welcome Back!");
                    session.setAttribute("admin", admin);
                    // Set the number of users, songs, singers of the session
                    ServletUtils.setNumber(session);
                }else {
                    jsonObject.put("Status", "Erro");
                    jsonObject.put("Erro", "Password Error");
                }
            }else {
                jsonObject.put("Status", "Erro");
                jsonObject.put("Erro", "The user does not exist");
            }
        }else {
            // Verification Error
            jsonObject.put("Status", "Erro");
            jsonObject.put("Erro", "Verification Error");
        }
        session.setAttribute("code", "");
        System.out.println(jsonObject.toString());

        ServletUtils.sendToPage(resp, jsonObject.toString());
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
