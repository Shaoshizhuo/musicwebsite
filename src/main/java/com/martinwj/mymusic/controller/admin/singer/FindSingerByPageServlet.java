package com.martinwj.mymusic.controller.admin.singer;

import com.martinwj.mymusic.entity.PageBean;
import com.martinwj.mymusic.entity.Singer;
import com.martinwj.mymusic.service.SingerService;
import com.martinwj.mymusic.service.impl.SingerServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Arrays;
import java.util.Map;

/**
 * @ClassName: FindSingerByPageServlet
 * @Description: TODO
 * @author: Shao Shizhuo
 * @createDate: 2023-1-21
 *
 */
@WebServlet("/admin/findSingerByPageServlet")
public class FindSingerByPageServlet extends HttpServlet {

    private SingerService singerService = null;

    @Override
    public void init() throws ServletException {
        singerService = new SingerServiceImpl();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        // Get parameters
        String currentPage = request.getParameter("currentPage");// Current Page
        String rows = request.getParameter("rows");// Number of items displayed per page

        if(currentPage == null || "".equals(currentPage)){
            currentPage = "1";
        }

        if(rows == null || "".equals(rows)){
            rows = "6";
        }

        //Get conditional query parameters
        Map<String, String[]> singerCondition = request.getParameterMap();

        //2.call service query
        PageBean<Singer> singerPb = singerService.findSingersByPage(currentPage, rows, singerCondition);
        System.out.println(singerPb);

        //3.Store PageBean in session
        HttpSession session = request.getSession();
        for(String s : singerCondition.keySet()){
            System.out.println(s + " : " + Arrays.toString(singerCondition.get(s)));
            session.setAttribute("singerKeyName", singerCondition.get(s)[0]);
        }
        session.setAttribute("singerPb", singerPb);
        session.setAttribute("singerCondition", singerCondition);// Store query conditions in request
        request.getRequestDispatcher("/page/manager/singer.jsp").forward(request, response);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
