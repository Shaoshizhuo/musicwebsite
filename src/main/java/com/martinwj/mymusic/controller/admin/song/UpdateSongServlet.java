package com.martinwj.mymusic.controller.admin.song;

import com.martinwj.mymusic.entity.Song;
import com.martinwj.mymusic.entity.Type;
import com.martinwj.mymusic.service.SongService;
import com.martinwj.mymusic.service.TypeService;
import com.martinwj.mymusic.service.impl.SongServiceImpl;
import com.martinwj.mymusic.service.impl.TypeServiceImpl;
import com.martinwj.mymusic.util.DateUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @ClassName: FindSongByPageServlet
 * @Description: TODO
 * @author: Shao Shizhuo
 * @createDate: 2023-1-22
 *
 */
@WebServlet("/admin/updateSongServlet")
public class UpdateSongServlet extends HttpServlet {

    private SongService songService = null;
    private TypeService typeService = null;

    @Override
    public void init() throws ServletException {
        songService = new SongServiceImpl();
        typeService = new TypeServiceImpl();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // get modification form
        String songId = request.getParameter("songId");
        String playerCount = request.getParameter("update_playerCount_" + songId);
        String downloadCount = request.getParameter("update_downloadCount_" + songId);
        String collectionCount = request.getParameter("update_collectionCount_" + songId);
        String type = request.getParameter("update_songType_" + songId);
        String initialType = request.getParameter("initialType_" + songId);
        String lyric = request.getParameter("updateLyric_" + songId);
        System.out.println(type);

        // encapsulated data
        Song song = new Song();
        song.setSongId(Integer.parseInt(songId));
        song.setPlayCount(Integer.parseInt(playerCount));
        song.setDownloadCount(Integer.parseInt(downloadCount));
        song.setCollectionCount(Integer.parseInt(collectionCount));
        song.setType(type);
        song.setLyric(lyric);
        song.setUpdateDate(DateUtils.getDateString());
        System.out.println(song);

        // Call the service to perform the modification operation
        int flag = songService.updateSong(song);

        // Determine whether the current type is consistent with the original type
        if(!type.equals(initialType)){
            String[] typeSplit = type.split(",");
            String[] initialSplitType = initialType.split(",");
            for(String s : typeSplit){
                // If the original classification string does not contain s, the number of songs of the modified type
                if(!initialType.contains(s)){
                    if(typeService.getTypeByName(s) != null){
                        typeService.updateTypeByName(s, true);
                    }else {
                        Type t = new Type();
                        t.setName(s);
                        t.setSongCount(1);
                        typeService.addType(t);
                    }
                }
            }
            for(String s : initialSplitType){
                if(!type.contains(s)) {
                    typeService.updateTypeByName(s, false);
                }
            }
        }

        // Jump to query all Servlets
        response.sendRedirect(request.getContextPath()+"/admin/findSongByPageServlet");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
