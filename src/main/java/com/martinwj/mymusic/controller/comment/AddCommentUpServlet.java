package com.martinwj.mymusic.controller.comment;

import com.martinwj.mymusic.service.CommentService;
import com.martinwj.mymusic.service.impl.CommentServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @ClassName: AddCommentUpServlet
 * @Description: TODO
 * @author: Shao Shizhuo
 * @createDate: 2023-1-12
 *
 */
@WebServlet("/addCommentUpServlet")
public class AddCommentUpServlet extends HttpServlet {

    private CommentService commentService = null;

    @Override
    public void init() throws ServletException {
        commentService = new CommentServiceImpl();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // Obtain parameters
        String commentId = request.getParameter("commentId");
        String songId = request.getParameter("songId");

        //
        int flag = commentService.addCommentUp(Integer.parseInt(commentId));

        response.sendRedirect(request.getContextPath() + "/commentMusicServlet?songId=" + songId);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
