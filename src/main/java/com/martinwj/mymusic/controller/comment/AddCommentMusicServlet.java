package com.martinwj.mymusic.controller.comment;

import com.martinwj.mymusic.entity.User;
import com.martinwj.mymusic.service.CommentService;
import com.martinwj.mymusic.service.impl.CommentServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @ClassName: AddCommentMusicServlet
 * @Description: TODO
 * @author: Shao Shizhuo
 * @createDate: 2023-1-12
 *
 */
@WebServlet("/user/person/addCommentMusicServlet")
public class AddCommentMusicServlet extends HttpServlet {

    private CommentService commentService = null;

    @Override
    public void init() throws ServletException {
        commentService = new CommentServiceImpl();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Get form parameters
        String commentMsg = request.getParameter("commentMsg");
        String songId = request.getParameter("songId");
        String songName = request.getParameter("songName");
        // Obtain user information
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        // If the user is not logged in, the login page is displayed
        if(user == null) {
            request.setCharacterEncoding("utf-8");
            System.out.println("User not logged in！");
            request.setAttribute("titleMsg", "Woops！");
            request.setAttribute("textMsg", "You have not logged in yet, please log in and try again");
            String url = request.getContextPath() + "/page/user/login.jsp";
            request.setAttribute("urlMsg", url);
            request.setAttribute("pageMsg", "Login");
            request.setAttribute("codeMsg", "");
            request.getRequestDispatcher("/page/user/message.jsp").forward(request, response);
            return;
        }

        int userId = user.getUserId();
        String username = user.getName();

        // Call service to add to the database
        int flag = commentService.addComment(userId, username, songId, songName, commentMsg);

        // Redirect to the comment-music.jsp page
        response.sendRedirect(request.getContextPath() + "/commentMusicServlet?songId=" + songId);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
