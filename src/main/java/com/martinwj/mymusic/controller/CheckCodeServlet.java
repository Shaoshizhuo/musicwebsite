package com.martinwj.mymusic.controller;

import com.martinwj.mymusic.util.ServletUtils;
import net.sf.json.JSONObject;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Random;

/**
 * @ClassName: checkCodeServlet
 * @Description: TODO
 * @author: Shao Shizhuo
 * @createDate: 2023-1-30
 *
 */
@WebServlet("/checkCodeServlet")
public class CheckCodeServlet extends HttpServlet {

    Random ran = new Random();
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghigklmnopqrstuvwxyz0123456789";

        StringBuilder code = new StringBuilder();
        for (int i = 1; i <= 4; i++) {
            int index = ran.nextInt(str.length());
            //Fetch character
            char ch = str.charAt(index);//Random character
            code.append(ch);
        }
        HttpSession session = request.getSession();
        session.setAttribute("code", code.toString());
        System.out.println(session.getAttribute("code"));

        JSONObject object = new JSONObject();
        object.put("code", code.toString());

//        response.setCharacterEncoding("utf-8");
//        response.setContentType("application/json; charset=utf-8");
//        PrintWriter writer = response.getWriter();
//        writer.print(object);
        ServletUtils.sendToPage(response, object.toString());

    }

    private void testCode(HttpServletResponse response) throws IOException {
        int width = 100;
        int height = 55;

        //1.Create an object, picture in memory (CapTcha picture object)
        BufferedImage image = new BufferedImage(width,height,BufferedImage.TYPE_INT_RGB);

        //2.Edit image
        //2.1 Background color
        Graphics g = image.getGraphics();
        g.setColor(Color.PINK);
        g.fillRect(0,0,width,height);

        //2.2 Draw the border
        g.setColor(Color.BLUE);
        g.drawRect(0,0,width - 1,height - 1);

        String str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghigklmnopqrstuvwxyz0123456789";
        //Generate random corner labels

        StringBuilder code = new StringBuilder();
        for (int i = 1; i <= 4; i++) {
            int index = ran.nextInt(str.length());
            char ch = str.charAt(index);
            g.setColor(randomColor());
            g.setFont(randomFont());
            //2.3 Write verification code
            g.drawString(ch+"",width/5*i,height/2);
            code.append(ch);
        }

        g.setColor(Color.GREEN);
        //Randomly generate coordinate points
        for (int i = 0; i < 10; i++) {
            int x1 = ran.nextInt(width);
            int x2 = ran.nextInt(width);
            int y1 = ran.nextInt(height);
            int y2 = ran.nextInt(height);
            g.drawLine(x1,y1,x2,y2);
        }

        for (int i = 0; i < 10; i++) {
            g.setColor(randomColor());
            int x1 = ran.nextInt(width);
            int y1 = ran.nextInt(height);
            g.drawOval(x1,y1,2,2);
        }
        System.out.println(code.toString());

        //3.Output the picture to the page display
        ImageIO.write(image,"jpg",response.getOutputStream());
    }


    //Create random color
    private Color randomColor(){

        int red = ran.nextInt(255);
        int green = ran.nextInt(255);
        int blue = ran.nextInt(255);
        return new Color(red,green,blue);
    }

    //生辰随机字体
    private Font randomFont(){
        String[] fontNames= {"宋体", "华文楷体", "黑体", "微软雅黑", "楷体_GB2312"};
        int index = ran.nextInt(fontNames.length);
        String fontName = fontNames[index];
        int style = ran.nextInt(4);
        int size=ran.nextInt(5)+ 24;
        return new Font(fontName,style,size);
    }

//    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        response.setContentType("image/jpeg");
//        response.setHeader("Pragma", "no-cache");
//        response.setHeader("Cache-Control", "no-cache");
//        response.setDateHeader("Expires", 0);
//        HttpSession session = request.getSession();
////        BufferedImage image = new BufferedImage(width,height,BufferedImage.TYPE_INT_RGB);
//
//        CreateImageCode vCode = new CreateImageCode(180,55,4,10);
//        String code = vCode.getCode();
//        session.setAttribute("code", code);
//        System.out.println(code);
//        vCode.write(response.getOutputStream());
//    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
