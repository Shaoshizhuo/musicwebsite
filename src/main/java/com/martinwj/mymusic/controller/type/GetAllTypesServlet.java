package com.martinwj.mymusic.controller.type;

import com.martinwj.mymusic.entity.Type;
import com.martinwj.mymusic.service.TypeService;
import com.martinwj.mymusic.service.impl.TypeServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @ClassName: GetAllTypesServlet
 * @Description: TODO
 * @author: Shao Shizhuo
 * @createDate: 2023-1-15
 *
 */
@WebServlet("/getAllTypesServlet")
public class GetAllTypesServlet extends HttpServlet {
    private TypeService typeService = null;

    @Override
    public void init() throws ServletException {
        typeService = new TypeServiceImpl();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Type> allTypes = typeService.getAllTypes();
        StringBuilder sb = new StringBuilder();
        for(Type type : allTypes){
            sb.append(type.getName()).append(",");
        }
        request.getSession().setAttribute("allTypes", allTypes);
        request.getSession().setAttribute("types", sb.toString());
        // Redirect to the category page
        response.sendRedirect(request.getContextPath() + "/page/user/categories.jsp");

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
