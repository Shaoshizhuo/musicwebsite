package com.martinwj.mymusic.util;

import it.sauronsoftware.jave.Encoder;
import it.sauronsoftware.jave.EncoderException;
import it.sauronsoftware.jave.MultimediaInfo;
import org.junit.Test;

import java.io.File;

/**
 * @ClassName: Tools
 * @Description: TODO
 * @author: Shao Shizhuo
 * @createDate: 2023-3-2
 *
 */
public class Tools {

    public static void main(String[] args) {
        String filename = "F:\\戴尔基地\\2019-06-02备份\\资源文件\\song\\周杰伦-告白气球.mp3";
        System.out.println(getMP3Timer(filename));
    }


    public static int getMP3Timer(String filename) {
        File source =new File(filename);
        Encoder encoder = new Encoder();
        MultimediaInfo m = null;
        try {
            m = encoder.getInfo(source);
        } catch (EncoderException e) {
            e.printStackTrace();
        }
        long ls = m.getDuration();
        long duration = (ls/1000);
        System.out.println("The Length of the video:"+ls/60000+"min"+(ls/1000-ls/60000*60)+"s！");
        return (int) duration;
    }

    @Test
    public void test() {
        String str = "123";
        System.out.println(str.substring(0, Math.min((str.length()), 5)));
    }



}
