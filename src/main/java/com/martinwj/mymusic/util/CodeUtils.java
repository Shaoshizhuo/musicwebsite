package com.martinwj.mymusic.util;

import java.util.Random;
import java.util.UUID;

/**
 * @ClassName: CodeUtils
 * @Description: TODO
 * @author: Shao Shizhuo
 * @createDate: 2023-2-26
 *
 */
public class CodeUtils {

    public static String getStringRandom(int length) {

        String val = "";
        Random random = new Random();

        // The parameter length indicates the number of random numbers generated
        for (int i = 0; i < length; i++) {

            String charOrNum = random.nextInt(2) % 2 == 0 ? "char" : "num";
            // Print letters or numbers
            if ("char".equalsIgnoreCase(charOrNum)) {
                // Whether the output is uppercase or lowercase letters
                int temp = random.nextInt(2) % 2 == 0 ? 65 : 97;
                val += (char) (random.nextInt(26) + temp);
            } else if ("num".equalsIgnoreCase(charOrNum)) {
                val += String.valueOf(random.nextInt(10));
            }
        }
        return val;
    }

    public static String getUUID() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

    public static void main(String[] args) {
        // Test
        System.out.println(getStringRandom(8));
        System.out.println(getUUID());
    }
}
