package com.martinwj.mymusic.util;

import com.martinwj.mymusic.service.SingerService;
import com.martinwj.mymusic.service.impl.SingerServiceImpl;
import com.martinwj.mymusic.service.impl.SongServiceImpl;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @ClassName: PageUtils
 * @Description: TODO
 * @author: Shao Shizhuo
 * @createDate: 2023-2-28
 *
 */
public class PageUtils {

    public static void getSql(StringBuilder sb, Map<String, String[]> condition, List<Object> parms){

        if(condition == null){
            return ;
        }

        // Iterate over the map collection
        Set<String> keySet = condition.keySet();
        for(String key : keySet){
            //Exclude the paging condition parameter
            if("currentPage".equals(key) || "rows".equals(key)){
                continue;
            }
            // Get value
            String value = condition.get(key)[0];
            //Judge if value is null
            if(value != null && !"".equals(value)) {
                if(key.equals("singerName")){
                    key = "singerId";
                    SingerService service = new SingerServiceImpl();
                    value = String.valueOf(service.getSingerBySingerName(value).getSingerId());
                }
                if(key.equals("accessCount") || key.equals("collectionCount")
                    || key.equals("downloadCount") || key.equals("playCount")){
                    sb.append(" and " + key + " >= ?");
                    parms.add(value);
                } else {
                    sb.append(" and " + key + " like ? ");
                    parms.add("%" + value + "%");
                }
            }
        }
    }

}
