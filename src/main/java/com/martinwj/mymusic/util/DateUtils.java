package com.martinwj.mymusic.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @ClassName: DateUtils
 * @Description: TODO
 * @author: Shao Shizhuo
 * @createDate: 2023-2-26
 *
 */
public class DateUtils {

    public static String getDateString(Date date){
        DateFormat dt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return dt.format(date);
    }

    public static String getDateString() {
        return getDateString(new Date());
    }
}
