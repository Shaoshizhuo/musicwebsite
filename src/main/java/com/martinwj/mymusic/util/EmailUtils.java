package com.martinwj.mymusic.util;

import java.util.Date;
import java.util.Properties;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;

/**
 * @ClassName: EmailUtils
 * @Description: TODO
 * @author: Shao Shizhuo
 * @createDate: 2023-2-26
 *
 */
public class EmailUtils {

    public static String myEmailSMTPHost = "smtp.qq.com";

    private static final String linkString = "http://localhost:8080";

    public static MimeMessage createMimeMessage(Session session, String receiveMail, String userRemarks,
                                                String senderName, String subjectText, String MailText, String EmailAccount) throws Exception {
        // 1. Create an Email
        MimeMessage message = new MimeMessage(session);

        // 2. From: sender
        message.setFrom(new InternetAddress(EmailAccount, senderName, "UTF-8"));

        // 3. To: receiver
        message.setRecipient(MimeMessage.RecipientType.TO, new InternetAddress(receiveMail, userRemarks, "UTF-8"));

        // 4. Subject: Title
        message.setSubject(subjectText, "UTF-8");

        // 5. Content: Content
        message.setContent(MailText, "text/html;charset=UTF-8");
        // 6. Set sent date
        message.setSentDate(new Date());

        // 7. save
        message.saveChanges();

        return message;
    }


    public static void Send(String EmailAccount, String EmailPassword, String receiveMail, String userRemarks,
                            String senderName, String subjectText, String mailText) {
        try {
            // 1.Create parameter Settings for connecting to the mail server
            Properties props = new Properties();
//            props.setProperty("mail.transport.protocol", "smtp");
          //  props.setProperty("mail.smtp.host", myEmailSMTPHost);
           // props.setProperty("mail.smtp.auth", "true");



            props.put("mail.transport.protocol", "smtp");
            props.put("mail.smtp.host", "smtp.qq.com");
            props.put("mail.smtp.port", 465);
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.ssl.enable", "true");
            props.put("mail.debug", "true");

            // 2. Create a session object based on the configuration to interact with the mail server
            Session session = Session.getInstance(props);
            // If the mode is set to debug, you can view detailed sending logs
            session.setDebug(true);

            // 3. Create a message
            MimeMessage message = createMimeMessage(session, receiveMail, userRemarks, senderName, subjectText,
                    mailText, EmailAccount);

            // 4. Gets the mail transfer object based on Session
            Transport transport = session.getTransport();

            // 5. Use your email account and password to connect to the email server
            transport.connect(EmailAccount, EmailPassword);

            // 6. Send the mail to all the recipient addresses
            transport.sendMessage(message, message.getAllRecipients());

            // 7. Close transport
            transport.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void sendMessage(HttpServletRequest request, String username, String code, String email) {
        String url  = "http://" + request.getServerName() + ":" + request.getServerPort() + "" + request.getContextPath() + "/user/activeServlet?username=" + username + "&code=" + code;
        String mailText = "<html><head></head><body><h1>This is an activation email, please click the link below to activate</h1><h3><a href='" + url +
               "'>" + url + "</a></h3></body></html>";

        System.out.println(mailText);
//        Send("2090728830@qq.com", "bnunfkwcgipddgia", email, "Remark", "OnlineMusic Website", "Active account", mailText);
        Send("1368289694@qq.com", "ksowyvukzafegigj", email, "Remark", "OnlineMusic Website", "Active account", mailText);
    }
    public static void main(String[] args) {
    	 Send("2090728830@qq.com", "bnunfkwcgipddgia", "urnotszwjoy@126.com", "Remark", "OnlineMusic Website", "Active account", "444");
	}
}
