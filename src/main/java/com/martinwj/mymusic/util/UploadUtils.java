package com.martinwj.mymusic.util;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;


/**
 * @ClassName: UploadUtils
 * @Description: TODO
 * @author: Shao Shizhuo
 * @createDate: 2023-3-2
 *
 */
public class UploadUtils {

    public static final String FORM_FIELDS = "form_fields";

    public static final String FILE_FIELDS = "file_fields";


    private long maxSize = 1000000;

    private Map<String, String> extMap = new HashMap<String, String>();

    private String basePath = "upload";
    private String dirName = "song";
    private static final String TEMP_PATH = "/temp";
    private String tempPath = basePath + TEMP_PATH;
    private String fileName;

    private String savePath;
    private String saveUrl;
    private String fileUrl;

    public UploadUtils() {

        extMap.put("images", "gif,jpg,jpeg,png,bmp");
        extMap.put("flashs", "swf,flv");
        extMap.put("medias", "swf,flv,mp3,wav,wma,wmv,mid,avi,mpg,asf,rm,rmvb");
        extMap.put("files", "doc,docx,xls,xlsx,ppt,htm,html,txt,zip,rar,gz,bz2");
    }


    @SuppressWarnings("unchecked")
    public String[] uploadFile(HttpServletRequest request) {
        String[] infos = new String[5];
        infos[0] = this.validateFields(request);
        Map<String, Object> fieldsMap = new HashMap<String, Object>();
        if (infos[0].equals("true")) {
            fieldsMap = this.initFields(request);
        }
        List<FileItem> fiList = (List<FileItem>) fieldsMap.get(UploadUtils.FILE_FIELDS);
        if (fiList != null) {
            for (FileItem item : fiList) {
                infos[1] = this.saveFile(item);
            }
            infos[2] = savePath;
            infos[3] = saveUrl;
            infos[4] = fileUrl;
        }
        return infos;
    }

    private String validateFields(HttpServletRequest request) {
        String errorInfo = "true";
        // boolean errorFlag = true;
        String contentType = request.getContentType();
        int contentLength = request.getContentLength();
        savePath = request.getSession().getServletContext().getRealPath("/") + basePath + "/";
        saveUrl = request.getContextPath() + "/" + basePath + "/";
        File uploadDir = new File(savePath);
        if (contentType == null || !contentType.startsWith("multipart")) {
            // TODO
            System.out.println("The request does not contain the multipart/form-data stream");
            errorInfo = "The request does not contain the multipart/form-data stream";
        } else if (maxSize < contentLength) {
            // TODO
            System.out.println("The size of the uploaded file exceeds the maximum file size");
            errorInfo = "The size of the uploaded file exceeds the maximum file size[" + maxSize + "]";
        } else if (!ServletFileUpload.isMultipartContent(request)) {
            // TODO
            errorInfo = "Please select file!";
        } else if (!uploadDir.isDirectory()) {// check directory
            // TODO
            errorInfo = "Directory[" + savePath + "] is not exist";
        } else if (!uploadDir.canWrite()) {
            // TODO
            errorInfo = "Directory[" + savePath + "] has no permission";
        } else if (!extMap.containsKey(dirName)) {
            // TODO
            errorInfo = "The directory name is incorrect";
        } else {

            savePath += dirName + "/";
            saveUrl += dirName + "/";
            File saveDirFile = new File(savePath);
            if (!saveDirFile.exists()) {
                saveDirFile.mkdirs();
            }
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
            String ymd = sdf.format(new Date());
            savePath += ymd + "/";
            saveUrl += ymd + "/";
            File dirFile = new File(savePath);
            if (!dirFile.exists()) {
                dirFile.mkdirs();
            }

            tempPath = request.getSession().getServletContext().getRealPath("/") + tempPath + "/";
            File file = new File(tempPath);
            if (!file.exists()) {
                file.mkdirs();
            }
        }

        return errorInfo;
    }


    @SuppressWarnings("unchecked")
    private Map<String, Object> initFields(HttpServletRequest request) {

        // Stores form fields and non-form fields
        Map<String, Object> map = new HashMap<String, Object>();

        //1. Judge request
        boolean isMultipart = ServletFileUpload.isMultipartContent(request);
        //2. Analyze request
        if (isMultipart) {
            // Create a factory for disk-based file items
            DiskFileItemFactory factory = new DiskFileItemFactory();

            // Threshold beyond which it will be written to the temporary directory, otherwise in memory
            factory.setSizeThreshold(1024 * 1024 * 10);
            factory.setRepository(new File(tempPath));

            // Create a new file upload handler
            ServletFileUpload upload = new ServletFileUpload(factory);

            upload.setHeaderEncoding("UTF-8");

            // Size max
            upload.setSizeMax(maxSize);

            /* FileItem */
            List<FileItem> items = null;
            // Parse the request
            try {
                items = upload.parseRequest(request);
            } catch (FileUploadException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            // 3. Deal with uploaded items
            if (items != null && items.size() > 0) {
                Iterator<FileItem> iter = items.iterator();
                List<FileItem> list = new ArrayList<FileItem>();
                Map<String, String> fields = new HashMap<String, String>();
                while (iter.hasNext()) {
                    FileItem item = iter.next();
                    if (item.isFormField()) {
                        String name = item.getFieldName();
                        String value = item.getString();
                        fields.put(name, value);
                    } else {
                        list.add(item);
                    }
                }
                map.put(FORM_FIELDS, fields);
                map.put(FILE_FIELDS, list);
            }
        }
        return map;
    }


    private String saveFile(FileItem item) {
        String error = "true";
        String fileName = item.getName();
        String fileExt = fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase();

        if (item.getSize() > maxSize) {
            // TODO
            error = "The size of the uploaded file exceeds the limit.";
        } else if (!Arrays.<String> asList(extMap.get(dirName).split(",")).contains(fileExt)) {
            error = "The file name extension is not allowed.\nOnly allow" + extMap.get(dirName) + "format.";
        } else {
            String newFileName;
            if ("".equals(fileName.trim())) {
                SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
                newFileName = df.format(new Date()) + "_" + new Random().nextInt(1000) + "." + fileExt;
            } else {
                newFileName = fileName + "." + fileExt;
            }

            fileUrl = saveUrl + newFileName;
            try {
                File uploadedFile = new File(savePath, newFileName);

                item.write(uploadedFile);

                /*
                 * FileOutputStream fos = new FileOutputStream(uploadFile); // 文件全在内存中 if (item.isInMemory()) { fos.write(item.get()); } else { InputStream is = item.getInputStream(); byte[] buffer =
                 * new byte[1024]; int len; while ((len = is.read(buffer)) > 0) { fos.write(buffer, 0, len); } is.close(); } fos.close(); item.delete();
                 */
            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("Upload Failure！");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return error;
    }


    public String getSavePath() {
        return savePath;
    }

    public String getSaveUrl() {
        return saveUrl;
    }

    public long getMaxSize() {
        return maxSize;
    }

    public void setMaxSize(long maxSize) {
        this.maxSize = maxSize;
    }

    public Map<String, String> getExtMap() {
        return extMap;
    }

    public void setExtMap(Map<String, String> extMap) {
        this.extMap = extMap;
    }

    public String getBasePath() {
        return basePath;
    }

    public void setBasePath(String basePath) {
        this.basePath = basePath;
        tempPath = basePath + TEMP_PATH;
    }

    public String getDirName() {
        return dirName;
    }

    public void setDirName(String dirName) {
        this.dirName = dirName;
    }

    public String getTempPath() {
        return tempPath;
    }

    public void setTempPath(String tempPath) {
        this.tempPath = tempPath;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

}
