package com.martinwj.mymusic.util;

import com.martinwj.mymusic.entity.Song;
import com.martinwj.mymusic.entity.Type;
import com.martinwj.mymusic.service.*;
import com.martinwj.mymusic.service.impl.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.HashMap;

/**
 * @ClassName: ServletUtils
 * @Description: TODO
 * @author: Shao Shizhuo
 * @createDate: 2023-3-2
 *
 */
public class ServletUtils {

    private static SongService songService = new SongServiceImpl();
    private static CommentService commentService = new CommentServiceImpl();
    private static TypeService typeService = new TypeServiceImpl();
    private static SingerService singerService = new SingerServiceImpl();
    private static UserService userService = new UserServiceImpl();

    public static void sendToPage(HttpServletResponse resp, String jsonStr) throws IOException {
        resp.setCharacterEncoding("utf-8");
        resp.setContentType("application/json; charset=utf-8");
        PrintWriter writer = resp.getWriter();
        writer.print(jsonStr);
    }

    public static void setNumber(HttpSession session) {
        // Update the number of types
        for(Type type : typeService.getAllTypes()) {
            type.setSongCount((int) songService.getAllSongsCountByType(type.getName()));
            typeService.updateTypeById(type);
        }
        session.setAttribute("singerNumber", singerService.getAllSingersCount(new HashMap<>()));
        session.setAttribute("songNumber", songService.getAllSongsCount(new HashMap<>()));
        session.setAttribute("commentNumber", commentService.getAllCommentsCount(new HashMap<>()));
        session.setAttribute("userNumber", userService.getAllUsersCount(new HashMap<>()));
    }

    public static void initSession(HttpServletRequest request){
        Enumeration em = request.getSession().getAttributeNames();
        while(em.hasMoreElements()){
            request.getSession().removeAttribute(em.nextElement().toString());
        }
    }

    public static void deleteSong(int id){
        Song song = null;

        song = songService.getSongById(id);
        String type = song.getType();
        commentService.delCommentBySongId(id);
        if(songService.deleteSongBySongId(id) > 0) {
            for (String str : type.split(",")) {
                typeService.updateTypeByName(str, false);
            }
        }
    }

    public static void deleteSong(String[] ids){
        for(String id : ids) {
            deleteSong(Integer.parseInt(id));
        }
    }
}
