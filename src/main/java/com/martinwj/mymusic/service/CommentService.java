package com.martinwj.mymusic.service;

import com.martinwj.mymusic.entity.Comment;
import com.martinwj.mymusic.entity.PageBean;
import com.martinwj.mymusic.entity.Song;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @ClassName: CommentService
 * @Description: TODO
 * @author: Shao Shizhuo
 * @createDate: 2023-2-21
 *
 */
public interface CommentService {

    List<Comment> getCommentsBySongId(int start, int rows, int songId);


    int getCommentsCountBySongId(int songId);


    int addComment(int userId, String username, String songId, String songName, String commentMsg);

    int addCommentUp(int commentId);

    int getAllCommentsCount(Map<String, String[]> condition);

    PageBean<Comment> findCommentsByPage(String currentPage, String rows, Map<String, String[]> condition);

    int updateCommentContext(int commentId, String newCommentContext);

    List<Integer> delCommentByCommentIds(String[] ids);

    int delCommentBySongId(int songId);

    List<Comment> getNewComment(int num);
}
