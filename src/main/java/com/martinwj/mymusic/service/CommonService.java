package com.martinwj.mymusic.service;

import com.martinwj.mymusic.dao.CommonDao;
import com.martinwj.mymusic.entity.PageBean;
import com.martinwj.mymusic.entity.Song;

import java.util.List;
import java.util.Map;

/**
 * @ClassName: CommonService
 * @Description: TODO
 * @author: Shao Shizhuo
 * @createDate: 2023-2-22
 *
 */
public abstract class CommonService<T> {

    public PageBean<T> findByPage(int currentPage, int rows, Map<String, String[]> condition, int totalCount, int start, List<T> list) {

        PageBean<T> pageBean = new PageBean<T>();
        pageBean.setCurrentPage(currentPage);
        pageBean.setRows(rows);

        pageBean.setTotalCount(totalCount);

        pageBean.setList(list);

        int totalPage = (totalCount % rows)  == 0 ? totalCount/rows : (totalCount/rows) + 1;
        pageBean.setTotalPage(totalPage);

        int startPage = 0, endPage = 0;
        if(totalPage > 5){
            if(currentPage > 3) {
                if (currentPage + 2 <= totalPage) {
                    startPage = currentPage - 2;
                    endPage = currentPage + 2;
                } else {
                    endPage = totalPage;
                    startPage = totalPage - 5;
                }
            }else {
                startPage = 1;
                endPage = 5;
            }
        } else {
            startPage = 1;
            endPage = totalPage;
        }

        pageBean.setStartPage(startPage);
        pageBean.setEndPage(endPage);

        return pageBean;

    }

}
