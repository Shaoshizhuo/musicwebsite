package com.martinwj.mymusic.service;

import com.martinwj.mymusic.entity.PageBean;
import com.martinwj.mymusic.entity.Singer;
import com.martinwj.mymusic.entity.Song;

import java.util.List;
import java.util.Map;

/**
 * @ClassName: SingerService
 * @Description: TODO
 * @author: Shao Shizhuo
 * @createDate: 2023-2-21
 *
 */
public interface SingerService {

    Singer getSingerBySingerId(int singerId);

    List<Singer> getSingerByEnglishName(String initials);

    Singer getSingerBySingerName(String singerName);

    long getAllSingersCount(Map<String, String[]> condition);


    PageBean<Singer> findSingersByPage(String currentPage, String rows, Map<String, String[]> condition);

    int updateSinger(Singer singer);


    List<Integer> deleteSingerBySingerId(String[] ids);


    int addSinger(Singer singer);

    List<Singer> getAllSingers();

    List<Singer> getSingerByName(String info);

    int updateSingerAccessCountBySingerName(String singerName);


    Singer getSingerBySongId(Integer songId);
}
