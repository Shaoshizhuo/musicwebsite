package com.martinwj.mymusic.service;

import com.martinwj.mymusic.entity.PageBean;
import com.martinwj.mymusic.entity.Song;
import com.martinwj.mymusic.entity.User;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @ClassName: UserService
 * @Description: TODO
 * @author: Shao Shizhuo
 * @createDate: 2023-2-23
 *
 */
public interface UserService {

    User getUserByUserId(int userId);

    User getUserByUsername(String username);


    User getUserByName(String name);


    List<User> getUserByEmail(String email);


    User getUserByPhone(String phone);


    int addUser(User user);

    int delUserByUsername(String username);


    User getUserByLogin(String username, String password);

    int updateUserActive(String username);

    PageBean<User> findUsersByPage(String currentPage, String rows, Map<String, String[]> condition);

    int getAllUsersCount(Map<String, String[]> condition);

    int updateUserPassword(int userId, String newPassword);

    List<Integer> delUserByUserIds(String[] ids);
}
