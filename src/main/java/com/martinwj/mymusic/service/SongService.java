package com.martinwj.mymusic.service;

import com.martinwj.mymusic.entity.PageBean;
import com.martinwj.mymusic.entity.Song;
import com.martinwj.mymusic.entity.Type;

import java.util.List;
import java.util.Map;

/**
 * @ClassName: SongService
 * @Description: TODO
 * @author: Shao Shizhuo
 * @createDate: 2023-2-21
 *
 */
public interface SongService {

    int addSong(Song song);

    Song getSongById(int id);


    long getAllSongsCount(Map<String, String[]> condition);

    PageBean<Song> findSongsByPage(String currentPage, String rows, Map<String, String[]> condition);


    int updateSong(Song song);

    List<Integer> deleteSongBySongId(String[] ids);

    Integer deleteSongBySongId(String id);

    Integer deleteSongBySongId(int id);


    Song getSongByName(String value);


    List<Song> getSongBySingerName(int start, int rows, String singerName);

    long getAllSongsCountBySingerName(String singerName);

    long getAllSongsCountByType(String songType);

    List<Song> getSongByType(int start, int rows, String songType);


    List<Song> getSongBySingerId(int singerId);


    List<Song> getSongByNameLike(String name);

    int addPlayCountBySongId(int songId);

    List<Song> getNewMusic(int i);

    List<Song> getSongByTypeWithRank(Type type, double playCountWeight, double downloadCountWeight, double collectionCountWeight, int num);
}
