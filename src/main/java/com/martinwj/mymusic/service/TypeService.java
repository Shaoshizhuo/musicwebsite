package com.martinwj.mymusic.service;

import com.martinwj.mymusic.entity.Type;

import java.util.List;

/**
 * @ClassName: TypeService
 * @Description: TODO
 * @author: Shao Shizhuo
 * @createDate: 2023-2-23
 *
 */
public interface TypeService {

    int addType(Type type);

    List<Type> getAllTypes();

    Type getTypeByName(String name);

    int delTypeById(int id);

    int delTypeByName(String name);

    int updateTypeById(Type type);

    int updateTypeByName(String name, boolean flag);

    List<Type> getTypesBySongCount(int i);
}
