package com.martinwj.mymusic.service;

import com.martinwj.mymusic.entity.Admin;

/**
 * @ClassName: AdminService
 * @Description: TODO
 * @author: Shao Shizhuo
 * @createDate: 2023-2-21
 *
 */
public interface AdminService {

    String usernameIsExit(String username);

    Admin getAdmin(String usernmae, String password);
}
