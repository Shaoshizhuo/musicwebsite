package com.martinwj.mymusic.service.impl;

import com.martinwj.mymusic.dao.AdminDao;
import com.martinwj.mymusic.dao.impl.AdminDaoImpl;
import com.martinwj.mymusic.entity.Admin;
import com.martinwj.mymusic.service.AdminService;

/**
 * @ClassName: AdminServiceImpl
 * @Description: TODO
 * @author: Shao Shizhuo
 * @createDate: 2023-2-20
 *
 */
public class AdminServiceImpl implements AdminService {

    // A reference to a DAO-layer object
    private AdminDao adminDao = null;

    public AdminServiceImpl(){
        adminDao = new AdminDaoImpl();
    }

    @Override
    public String usernameIsExit(String username) {
        return adminDao.usernameIsExit(username);
    }

    @Override
    public Admin getAdmin(String usernmae, String password) {
        // Encapsulate admin object
        Admin admin = new Admin(0, usernmae, password, null, null);
        return adminDao.getAdmin(admin);
    }
}
