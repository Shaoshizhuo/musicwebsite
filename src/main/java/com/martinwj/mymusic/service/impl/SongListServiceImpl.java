package com.martinwj.mymusic.service.impl;

import com.martinwj.mymusic.dao.SongListDao;
import com.martinwj.mymusic.dao.impl.SongListDaoImpl;
import com.martinwj.mymusic.service.SongListService;

/**
 * @ClassName: SongListServiceImpl
 * @Description: TODO
 * @author: Shao Shizhuo
 * @createDate: 2023-2-20
 *
 */
public class SongListServiceImpl implements SongListService {

    private SongListDao songListDao = null;

    public SongListServiceImpl() {
        songListDao = new SongListDaoImpl();
    }

}
