package com.martinwj.mymusic.dao.impl;

import com.martinwj.mymusic.dao.BaseDao;
import com.martinwj.mymusic.dao.SongListDao;
import com.martinwj.mymusic.entity.SongList;

/**
 * @ClassName: SongListDaoImpl
 * @Description: TODO
 * @author: Shao Shizhuo
 * @createDate: 2023-1-25
 *
 */
public class SongListDaoImpl extends BaseDao<SongList> implements SongListDao {

}
