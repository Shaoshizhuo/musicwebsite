package com.martinwj.mymusic.dao;

import com.martinwj.mymusic.entity.User;

import java.util.List;
import java.util.Map;

/**
 * @ClassName: UserDao
 * @Description: TODO
 * @author: Shao Shizhuo
 * @createDate: 2023-2-6
 *
 */
public interface UserDao {


    User getUserByUsername(String username);


    User getUserByName(String name);


    List<User> getUserByEmail(String email);


    User getUserByPhone(String phone);

    int addUser(User user);


    int delUserByUsername(String username);


    User getUserByLogin(Object[] parms);


    int updateUserActive(String username);


    User getUserByUserId(int userId);

    long getAllUsersCount(Map<String, String[]> condition);


    List<User> findUsersByPage(int start, int rows, Map<String, String[]> condition);


    int updateUserPassword(int userId, String newPassword);

    Integer delUserByUserId(int id);
}
