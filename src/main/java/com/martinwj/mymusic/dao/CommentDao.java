package com.martinwj.mymusic.dao;

import com.martinwj.mymusic.entity.Comment;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @ClassName: CommentDao
 * @Description: TODO
 * @author: Shao Shizhuo
 * @createDate: 2023-2-3
 *
 */
public interface CommentDao {

    List<Comment> getCommentsBySongId(int start, int rows, int songId);


    int getCommentsCountBySongId(int songId);

    int addComment(Comment comment);

    int addCommentUp(int commentId);

    int getAllCommentsCount(Map<String, String[]> condition);

    List<Comment> findCommentsByPage(int start, int rows, Map<String, String[]> condition);


    int updateCommentContext(int commentId, String newCommentContext);

    Integer delCommentByCommentId(int commentId);


    int delCommentBySongId(int songId);

    List<Comment> getNewComment(int num);
}
