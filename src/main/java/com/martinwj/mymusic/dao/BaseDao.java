package com.martinwj.mymusic.dao;

import com.martinwj.mymusic.util.JdbcUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * @ClassName: AdminDao
 * @Description: TODO
 * @author: Shao Shizhuo
 * @createDate: 2023-2-1
 *
 */
public abstract class BaseDao<T> {

    //Use DbUtils to modify database
    private QueryRunner queryRunner = new QueryRunner();


    private Class<T> type;

    // Gets the Class object of T, gets the type of the generic, which is determined when it is inherited by the subclass
    public BaseDao() {
        Class clazz = this.getClass();
        ParameterizedType parameterizedType = (ParameterizedType) clazz.getGenericSuperclass();
        Type[] types = parameterizedType.getActualTypeArguments();
        this.type = (Class<T>) types[0];
    }

    // Universal add, delete and modify operations
    public int update(Connection conn, String sql, Object... params) {
        int count = 0;
        try {
            count = queryRunner.update(conn, sql, params);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }

    // Universal add, delete and modify operations
    public int update(String sql, Object... params) {
        Connection conn = JdbcUtils.getConnection();
        return update(conn, sql, params);
    }


    // Get an object
    public T getBean(Connection conn, String sql, Object... params) {
        T t = null;
        try {
            t = queryRunner.query(conn, sql, new BeanHandler<T>(type), params);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return t;
    }

    // Provides a connection method for getting an object
    public T getBean(String sql, Object... params){
        System.out.println(" BaseDao progress in [" +Thread.currentThread().getName() + "]");
        Connection conn = JdbcUtils.getConnection();
        return getBean(conn, sql, params);
    }

    // Get all objects
    public List<T> getBeanList(Connection conn, String sql, Object... params) {
        List<T> list = null;
        try {
            list = queryRunner.query(conn, sql, new BeanListHandler<T>(type), params);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    // Provides linked methods to get all objects
    public List<T> getBeanList(String sql, Object... params) {
        Connection conn = JdbcUtils.getConnection();
        return getBeanList(conn, sql, params);
    }

    public Object getValue(Connection conn,String sql, Object... params) {
        Object count = null;
        try {
            // Call queryRunner's query method to get a single value
            count = queryRunner.query(conn,
                    sql,
                    new ScalarHandler(),
                    params);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }

    //Gets a single value method that is used specifically to perform operations like select count(*)...

    public Object getValue(String sql, Object... params) {
        Connection conn = JdbcUtils.getConnection();
        return getValue(conn, sql, params);
    }

}
