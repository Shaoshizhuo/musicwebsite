package com.martinwj.mymusic.dao;

import com.martinwj.mymusic.entity.Type;

import java.util.List;

/**
 * @ClassName: TypeDao
 * @Description: TODO
 * @author: Shao Shizhuo
 * @createDate: 2023-2-6
 *
 */
public interface TypeDao {


    int addType(Type type);


    List<Type> getAllTypes();


    Type getTypeByName(String name);


    int delTypeById(int id);


    int delTypeByName(String name);

    int updateTypeById(Type type);


    int updateTypeByName(String name, boolean flag);


    List<Type> getTypesBySongCount(int i);
}
