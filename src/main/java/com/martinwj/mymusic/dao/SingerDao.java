package com.martinwj.mymusic.dao;

import com.martinwj.mymusic.entity.Singer;

import java.util.List;
import java.util.Map;

/**
 * @ClassName: SingerDao
 * @Description: TODO
 * @author: Shao Shizhuo
 * @createDate: 2023-2-6
 *
 */
public interface SingerDao {

    Singer getSingerBySingerId(int singerId);

    List<Singer> getSingerByEnglishName(String initials);


    Singer getSingerBySingerName(String singerName);


    long getAllSingersCount(Map<String, String[]> condition);


    List<Singer> findSingersByPage(int start, int rows, Map<String, String[]> condition);


    int updateSinger(Singer singer);

    int deleteSingerBySIngerId(int singerId);


    int addSinger(Singer singer);


    List<Singer> getAllSingers();


    List<Singer> findSingersByName(String info);


    int updateSingerAccessCountBySingerName(String singerName);

    Singer getSingerBySongId(Integer songId);
}
