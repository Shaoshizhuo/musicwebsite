package com.martinwj.mymusic.dao;

import com.martinwj.mymusic.entity.Song;
import com.martinwj.mymusic.entity.Type;

import java.util.List;
import java.util.Map;

/**
 * @ClassName: SongDao
 * @Description: TODO
 * @author: Shao Shizhuo
 * @createDate: 2023-2-6
 *
 */
public interface SongDao {


    int addSong(Song song);


    Song findSongById(int id);

    List<Song> findSongBySingerId(int singerId);


    Long getAllSongsCount(Map<String, String[]> condition);


    List<Song> findSongsByPage(int start, int rows, Map<String, String[]> condition);


    int deleteSong(int songId);


    int updateSong(Song song);


    Song findSongByName(String value);


    List<Song> findSongBySingerName(int start, int rows, String singerName);


    long getAllSongsCountBySingerName(String singerName);


    long findSongsCountByType(String songType);


    List<Song> findSongsByType(int start, int rows, String songType);


    List<Song> findSongByNameLike(String name);


    int addPlayCountBySongId(int songId);


    List<Song> getNewMusic(int i);

    List<Song> getSongByTypeWithRank(Type type, double playCountWeight, double downloadCountWeight, double collectionCountWeight, int num);
}
