package com.martinwj.mymusic.dao;

import com.martinwj.mymusic.entity.Song;
import com.martinwj.mymusic.util.PageUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @ClassName: CommonDao
 * @Description: TODO
 * @author: Shao Shizhuo
 * @createDate: 2023-2-3
 *
 */
public abstract class CommonDao<T> extends BaseDao<T> {

    public long getAllCount(String sql, Map<String, String[]> condition) {
        StringBuilder sb = new StringBuilder(sql);
        // Define parameter set
        List<Object> parms = new ArrayList<Object>();

        PageUtils.getSql(sb, condition, parms);

        System.out.println(sb.toString());
        System.out.println(parms);

        return (long) getValue(sb.toString(), parms.toArray());
    }

    /**
     * Paging condition query
     * @param start Current page
     * @param rows Number of data items per page
     * @param condition
     * @return
     */
    public List<T> findByPage(String sql, int start, int rows, Map<String, String[]> condition) {
        StringBuilder sb = new StringBuilder(sql);
        // Define parameter set
        List<Object> parms = new ArrayList<Object>();

        PageUtils.getSql(sb, condition, parms);

        sb.append(" limit ?,? ");
        parms.add(start);
        parms.add(rows);
        sql = sb.toString();

        System.out.println(sb.toString());
        System.out.println(parms);

        return getBeanList(sb.toString(), parms.toArray());
    }
}
