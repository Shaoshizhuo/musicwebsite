package com.martinwj.mymusic.dao;

import com.martinwj.mymusic.entity.Admin;

/**
 * @ClassName: AdminDao
 * @Description: TODO
 * @author: Shao Shizhuo
 * @createDate: 2023-2-1
 *
 */
public interface AdminDao {

    /**
     * Check whether the user name exists
     * @param username
     * @return
     */
    String usernameIsExit(String username);

    /**
     * The administrator logs in and obtains the Admin object through the username and password entered by the user.
     * @param admin
     * @return
     */
    Admin getAdmin(Admin admin);

}
