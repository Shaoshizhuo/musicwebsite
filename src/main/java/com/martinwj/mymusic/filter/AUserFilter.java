package com.martinwj.mymusic.filter;

import com.martinwj.mymusic.entity.User;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @ClassName: UserFilter
 * @Description: TODO
 * @author: Shao Shizhuo
 * @createDate: 2023-2-10
 *
 */
@WebFilter("/page/user/person/*")
public class AUserFilter implements Filter {
    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        if(user == null){
            request.setCharacterEncoding("utf-8");
            System.out.println("User not logged in！");
            response.sendRedirect(request.getContextPath() + "/page/user/login.jsp");
            return;
        } else {
            System.out.println("User logged in！");
            chain.doFilter(req, resp);
        }
    }

    public void init(FilterConfig config) throws ServletException {

    }

}
