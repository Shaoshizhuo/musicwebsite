<%@ page import="java.util.Random" %>
<%@ include file="_pre.jsp"%>
<%--
  Created by IntelliJ IDEA.
  Creater: Shao Shizhuo
  Date: 2023/2/5
  Time: 15:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="${pageContext.request.contextPath }/page/user/assets/img/basic/favicon.ico" type="image/x-icon">
    <title>Music</title>
    <!-- CSS -->
    <link rel="stylesheet" href="${pageContext.request.contextPath }/page/user/assets/css/app.css">
</head>
<%
    Random random = new Random();
    int randValue = random.nextInt(5);
    request.setAttribute("randValue", randValue);
%>
<body class="sidebar-mini sidebar-collapse theme-dark  sidebar-expanded-on-hover has-preloader" style="background-color:#FFFFFF">

<!--</nav>-->
<!--Page Content-->
<main id="pageContent" class="page has-sidebar">
    <div class="container-fluid relative animatedParent animateOnce no-p" style="background-color:#FFFFFF;">
        <div class="animated fadeInUpShort">
            <!--Banner Slider-->
            <section>
                <div class="text-white">
                    <div class="lightSlider"
                         data-item="1"
                         data-controls="true"
                         data-slide-margin="0"
                         data-gallery="false"
                         data-pause="8000"
                         data-pauseonhover="true"
                         data-auto="false"
                         data-pager="false"
                         data-loop="true">
                        <div class="xv-slide" data-bg-possition="top"
                             style="background-image:url('assets/img/demo/bg5.jpg');">
                            <div class="has-bottom-gradient">
                                <div class="p-md-5 p-3">
                                    <div class="row">
                                        <div class="col-12 col-lg-6 fadeInRight animated">
                                            <div class="xv-slider-content clearfix color-white">
                                                <h1 class="s-64 mt-5 font-weight-lighter">Music</h1>
                                                <p class="s-14 font-weight-lighter">
                                                    <c:if test="${requestScope.randValue == 0}">
                                                        Start your music Travel
                                                    </c:if>
                                                    <c:if test="${requestScope.randValue == 1}">
                                                        Start your music Travel
                                                    </c:if>
                                                    <c:if test="${requestScope.randValue == 2}">
                                                        Start your music Travel
                                                    </c:if>
                                                    <c:if test="${requestScope.randValue == 3}">
                                                        Start your music Travel
                                                    </c:if>
                                                    <c:if test="${requestScope.randValue == 4}">
                                                        Start your music Travel
                                                    </c:if>
                                                    <c:if test="${requestScope.randValue == 5}">
                                                        Start your music Travel
                                                    </c:if>
                                                </p>
                                                <div class="pt-3">
                                                    <a href="javascript: toPlayerList();" class="btn btn-primary btn-lg">Welcome!</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="bottom-gradient"></div>
                        </div>
                    </div>
                </div>
                <!--slider Wrap-->
            </section>
            <!--@Banner Slider-->
            <div class="p-md-5 p-3  " style="background-color:#FFFFFF;">
                <!--New Releases-->
                <section class="section">
                    <div class="d-flex relative align-items-center justify-content-between">
                        <div class="mb-4">
                            <h4>Popular Songs</h4>
                            <p>Click to Listen！</p>
                        </div>
                        <a href="${pageContext.request.contextPath}/getAllSingerServlet">View more<i class="icon-angle-right ml-3"></i></a>
                    </div>
                    <div class="lightSlider has-items-overlay playlist"
                         data-item="6"
                         data-item-lg="3"
                         data-item-md="3"
                         data-item-sm="2"
                         data-auto="false"
                         data-pager="false"
                         data-controls="true"
                         data-loop="false">

                        <c:forEach items="${sessionScope.singerSongMap}" var="map" varStatus="s">
                            <div>
                                <figure>
                                    <div class="img-wrapper">
                                        <img src="${pageContext.request.contextPath}/${map.key.pic}" alt="/">
                                        <div class="img-overlay text-white">
                                            <div class="figcaption">
                                                <ul class="list-inline d-flex align-items-center justify-content-between">
                                                    <li class="list-inline-item">
                                                        <a href="#" class="snackbar" data-text="Added to favourites"
                                                           data-pos="top-right"
                                                           data-showAction="true"
                                                           data-actionText="ok"
                                                           data-actionTextColor="#fff"
                                                           data-backgroundColor="#0c101b">
                                                            <i class="icon-heart-o s-18"></i>
                                                        </a>
                                                    </li>
                                                    <li class="list-inline-item ">
                                                        <a class="no-ajaxy media-url" href="${pageContext.request.contextPath}/${map.value.url}" data-wave="assets/media/track2.json">
                                                            <i class="icon-play s-48"  onclick="addPlayCount(${map.value.songId})"></i>
                                                        </a>
                                                    </li>
                                                    <li class="list-inline-item">
                                                        <a href="${pageContext.request.contextPath}/commentMusicServlet?songId=${map.value.songId}"><i
                                                                class="icon-more s-18 pt-3"></i></a></li>
                                                </ul>
                                                <div class="text-center mt-5">
                                                    <%-- Song Name --%>
                                                    <h5>${map.value.name}</h5>
                                                    <%-- Singer Name --%>
                                                    <span>${map.key.name}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="figure-title text-center p-2">
                                                <%-- Song Name --%>
                                            <h5>${map.value.name}</h5>
                                                <%-- Singer Name --%>
                                            <span>${map.key.name}</span>
                                        </div>
                                    </div>
                                </figure>
                            </div>
                        </c:forEach>
                    </div>
                </section>
                <!--@New Releases-->

                <!--Latest Posts-->
                <section class="section">
                    <div class="d-flex relative">
                        <div class="mb-4">
                            <h4>Latest Comments</h4>
                            <p>View the latest comments of songs!</p>
                        </div>
                    </div>
                    <div class="lightSlider has-items-overlay"
                         data-item="3"
                         data-item-lg="2"
                         data-item-md="1"
                         data-item-sm="1"
                         data-auto="false"
                         data-pager="false"
                         data-controls="true"
                         data-loop="false">
                        <c:forEach items="${sessionScope.comments}" var="comment" varStatus="s">
                            <div class="card">
                                <figure class="card-img figure">
                                    <div class="img-wrapper">
                                        <img src="${pageContext.request.contextPath}/page/user/assets/img/demo/v<%=new Random().nextInt(10) + 1%>.jpg" alt="Card image">
                                    </div>
                                    <div class="img-overlay"></div>
                                    <div class="has-bottom-gradient">
                                        <div class="d-flex">
                                            <div class="card-img-overlay">
                                                <div class="pt-3 pb-3">
                                                    <a href="${pageContext.request.contextPath}/commentMusicServlet?songId=${comment.songId}">
                                                        <figure class="float-left mr-3 mt-1">
                                                            <i class="icon-play s-36"></i>
                                                        </figure>
                                                        <div>
                                                            <h5>
                                                                    ${comment.songName}
                                                            </h5>
                                                            <small>Latest Comments：${comment.context.substring(0, comment.context.length() >= 10 ? 10 : map.value.context.length())}...</small>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </figure>
                                <div class="bottom-gradient bottom-gradient-thumbnail"></div>
                            </div>
                        </c:forEach>
                    </div>
                </section>
                <!--@New Releases-->

                <!--PlayList & Events-->
                <section id="songRankList" class="section mt-4" >
                    <div class="row row-eq-height" style="background-color:#FFFFFF;">
                        <div class="col-lg-8" style="background-color:#FFFFFF;">
                            <div class="card no-b mb-md-3 p-2" >
                                <div class="card-header no-bg transparent">
                                    <div class="d-flex justify-content-between">
                                        <div class="align-self-center">
                                            <div class="d-flex" >
                                                <!--<i class="icon-music s-36 mr-3  mt-2"></i>-->
                                                <div>
                                                    <h4>Week Top</h4>
                                                    <p>Listen the top songs of this week!</p>
                                                    <div class="mt-3">
                                                        <ul class="nav nav-tabs card-header-tabs nav-material responsive-tab mb-1"
                                                            role="tablist">
                                                            <c:forEach items="${sessionScope.typeRankList}" var="type" varStatus="s">
                                                                <li class="nav-item">
                                                                    <a  id="w${s.count}--tab${s.count}"
                                                                       data-toggle="tab"
                                                                       href="#w${s.count}-tab${s.count}"
                                                                       role="tab"
                                                                       <c:if test="${s.count == 1}">
                                                                           class="nav-link active show"
                                                                           aria-selected="true"
                                                                       </c:if>
                                                                       <c:if test="${s.count != 1}">
                                                                           class="nav-link"
                                                                           aria-selected="false"
                                                                       </c:if>
                                                                    >${type.name}</a>
                                                                </li>
                                                            </c:forEach>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body no-p">
                                    <div class="tab-content" id="v-pills-tabContent1">
                                        <c:forEach items="${sessionScope.songRankList}" var="songList" varStatus="status">
                                            <div id="w${status.count}-tab${status.count}" role="tabpanel"
                                                <c:if test="${status.count == 1}">
                                                    class="tab-pane fade show active"
                                                </c:if>
                                                <c:if test="${status.count != 1}">
                                                    class="tab-pane fade"
                                                </c:if>
                                                 aria-labelledby="w${status.count}-tab${status.count}">

                                                <c:if test="${status.count % 2 != 0}">
                                                    <div class="playlist pl-lg-3 pr-lg-3">
                                                        <c:forEach items="${songList}" var="songMap" varStatus="s">
                                                            <div class="m-1 my-4">
                                                                <div class="d-flex align-items-center">
                                                                    <div class="col-1">
                                                                        <a class="no-ajaxy media-url"
                                                                           href="${pageContext.request.contextPath}/${songMap.value.url}"
                                                                           data-wave="assets/media/track3.json">
                                                                            <i class="icon-play s-28" onclick="addPlayCount(${songMap.value.songId})"></i>
                                                                        </a>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <figure class="avatar-md float-left  mr-3 mt-1">
                                                                            <img class="r-3" src="${pageContext.request.contextPath}/${songMap.key.pic}" alt="">
                                                                        </figure>
                                                                        <h6>${songMap.value.name}</h6>${songMap.key.name}
                                                                    </div>
                                                                    <div class="col-md-5 d-none d-lg-block">
                                                                        <div class="d-flex">
                                                                    <span class="ml-auto">
                                                                        <fmt:formatNumber value="${(songMap.value.time - songMap.value.time % 60) / 60}" type="number" pattern="#" maxFractionDigits="0"/>:
                                                                        <fmt:formatNumber value="${songMap.value.time % 60}"/>
                                                                    </span>
                                                                            <a href="${pageContext.request.contextPath}/addMusicToPlayListServlet?page=${pageContext.request.servletPath}&songId=${songMap.value.songId}" class="ml-auto"><i class="icon-add-1"></i></a>
                                                                            <div class="ml-auto">
                                                                                <a href="javascript:download(${songMap.value.songId})" class="btn btn-outline-primary btn-sm">Download</a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-1 ml-auto d-lg-none">
                                                                        <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                                                                            <i class="icon-more-1"></i></a>
                                                                        <div class="dropdown-menu dropdown-menu-right">
                                                                            <a  class="dropdown-item" href="#"><i class="icon-share-1 mr-3"></i> Share</a>
                                                                            <a  class="dropdown-item" href="#"><i class="icon-shopping-bag mr-3"></i>Buy at iTunes</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </c:forEach>
                                                    </div>
                                                </c:if>

                                                <c:if test="${status.count % 2 == 0}">
                                                    <div class="card-body has-items-overlay playlist p-5">
                                                        <div class="row">
                                                            <c:forEach items="${songList}" var="songMap" varStatus="s">
                                                                <div class="col-md-3 mb-3">
                                                                    <figure class="mb-2">
                                                                        <div class="img-wrapper r-10">
                                                                            <img class=" r-10" src="${pageContext.request.contextPath}/${songMap.key.pic}"
                                                                                 alt="/">
                                                                            <div class="img-overlay text-white p-5">
                                                                                <div class="center-center">
                                                                                    <a class="no-ajaxy media-url"
                                                                                       href="${pageContext.request.contextPath}/${songMap.value.url}"
                                                                                       data-wave="assets/media/track3.json">
                                                                                        <i class="icon-play s-48" onclick="addPlayCount(${songMap.value.songId})"></i>
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </figure>
                                                                    <div class="figure-title">
                                                                        <h6>${songMap.value.name}</h6>
                                                                        <small>${songMap.key.name}</small>
                                                                    </div>
                                                                </div>
                                                            </c:forEach>
                                                        </div>
                                                    </div>
                                                </c:if>

                                            </div>
                                        </c:forEach>

                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                </section>

            </div>
        </div>
    </div>
</main><!--@Page Content-->
<!--</div>&lt;!&ndash;@#app&ndash;&gt;-->
<!--/#app -->
<script src="${pageContext.request.contextPath }/page/user/assets/js/app.js"></script>
<script type="text/javascript">
    function download(songId) {
        window.location.href = "${pageContext.request.contextPath}/user/person/downLoadServlet?songId=" + songId;
    }

    function toPlayList() {
        $("#songRankList").scrollIntoView();
    }
</script>

</body>
</html>
