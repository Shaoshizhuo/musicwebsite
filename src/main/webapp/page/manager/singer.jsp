<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%--
  Created by IntelliJ IDEA.
  Creater: Shao Shizhuo
  Date: 2023/1/13
  Time: 22:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
    <link rel="icon" href="favicon.ico" type="image/ico">
    <title>SInger Manage</title>
    <link href="${pageContext.request.contextPath }/css/manager/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath }/css/manager/fonts.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath }/css/manager/style.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath }/css/manager/animate.css" rel="stylesheet">
    <script type="text/javascript" src="${pageContext.request.contextPath }/js/jstool/tool.js"></script>
    <link rel="stylesheet" href="${pageContext.request.contextPath }/js/manager/jquery-tags-input/jquery.tagsinput.min.css">
</head>

<%
    String message = (String) session.getAttribute("msg");
    request.setAttribute("message", message);
    session.setAttribute("msg", "");
%>

<body onload="showMsg(${requestScope.message})">
<!--Main-->
<main class="ftdms-layout-content">

    <div class="container-fluid" style="margin-bottom:90px;">

        <div class="row" style="margin-top:15px;">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-toolbar clearfix">
                        <form class="pull-right search-bar" method="get" action="#" role="form">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <input type="text" hidden name="search_field" id="search-field" value="name"/>
                                    <button class="btn btn-default dropdown-toggle" id="search-btn" data-toggle="dropdown" type="button" aria-haspopup="true" aria-expanded="false">
                                        Singer Name <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li> <a tabindex="-1" href="javascript:void(0)" data-field="name">Singer Name</a> </li>
                                        <li> <a tabindex="-1" href="javascript:void(0)" data-field="sex">Gender</a> </li>
                                        <li> <a tabindex="-1" href="javascript:void(0)" data-field="location">Country</a> </li>
                                        <li> <a tabindex="-1" href="javascript:void(0)" data-field="accessCount">Minimum heat</a> </li>
                                        <li> <a tabindex="-1" href="javascript:void(0)" data-field="collectionCount">Minimum Save</a> </li>
                                    </ul>
                                </div>
                                <input type="text" class="form-control" value="${sessionScope.singerKeyName}" id="keyName" name="keyName" >
                            </div>
                        </form>
                        <div class="toolbar-btn-action">
                            <a class="btn submenuitem btn-success m-r-5 " href="${pageContext.request.contextPath}/admin/findSingerByPageServlet?currentPage=${sessionScope.singerPb.currentPage}&rows=6&name=${sessionScope.singerCondition.name[0]}&sex=${sessionScope.singerCondition.sex[0]}&location=${sessionScope.singerCondition.location[0]}" data-id="link553" data-index="553"> Refresh</a>
                            <a class="btn submenuitem btn-primary m-r-5 " href="${pageContext.request.contextPath}/page/manager/addsinger.jsp" data-id="link552" data-index="552"> Add</a>
                            <a class="btn submenuitem btn-danger" href="javascript:delSelected();" data-id="link555" data-index="555"> Delete</a>
                        </div>
                    </div>
                    <div class="card-body">

                        <div class="table-responsive">
<%--                            <form id="form_del" method="post">--%>
                                <table class="table table-bordered" >
                                    <thead>
                                    <tr>
                                        <th width="5">
                                            <label class="ftdms-checkbox checkbox-primary">
                                                <input type="checkbox" id="check-all"><span></span>
                                            </label>
                                        </th>
                                        <th>No.</th>
                                        <th>Singer Name</th>
                                        <th>Gender</th>
                                        <th>Birth</th>
                                        <th>Country</th>
                                        <th>Initial Heat</th>
                                        <th>Save Times</th>
                                        <th>Debut Date</th>
                                        <th>Operation</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach items="${sessionScope.singerPb.list}" var="singer" varStatus="s">
                                        <tr>
                                            <td>
                                                <label class="ftdms-checkbox checkbox-primary">
                                                    <input type="checkbox" id="singerId" name="singerId" value="${singer.singerId}"/><span></span>
                                                </label>
                                            </td>
                                            <td>${s.count}</td>
                                            <td>${singer.name}</td>
                                            <td>${singer.sex == 0 ? "Female" : singer.sex == 1 ? "Male" : singer.sex == 2 ? "Group" : "Other"}</td>
                                            <td>${singer.birth}</td>
                                            <td>${singer.location}</td>
                                            <td>${singer.accessCount}</td>
                                            <td>${singer.collectionCount}</td>
                                            <td>${singer.debutDate}</td>
                                            <td>
                                                <div class="btn-group">

                                                    <a type="button" class="btn btn-primary btn-xs btn-default submenuitem" data-toggle="modal" data-target="#exampleModal_${singer.singerId}" data-whatever="@mdo" target= "_self" title="Edit"><i class="ftsucai-edit-2"></i></a>
                                                    <div class="modal fade" id="exampleModal_${singer.singerId}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                    <h4 class="modal-title" id="exampleModalLabel_${singer.singerId}">Edit Singer<${singer.name}>Information：</h4>
                                                                </div>
                                                                <form class="form_update_${singer.singerId}" onsubmit="return updateSinger(${singer.singerId})" action="${pageContext.request.contextPath}/admin/updateSingerServlet?singerId=${singer.singerId}" method="post">
                                                                    <div class="modal-body">
                                                                        <input type="hidden" id="collectionCount_${singer.singerId}" name="update_collectionCount_${singer.singerId}" value="${singer.collectionCount}"/>
                                                                        <div class="form-group">
                                                                            <label for="update_singerLocation_${singer.singerId}" class="control-label">Country：</label>
                                                                            <input class="form-control" type="text" id="update_singerLocation_${singer.singerId}" name="update_singerLocation_${singer.singerId}"  value="${singer.location}"/>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label for="update_accessCount_${singer.singerId}" class="control-label">Initial Heat：</label>
                                                                            <input class="form-control" type="text" id="update_accessCount_${singer.singerId}" name="update_accessCount_${singer.singerId}" value="${singer.accessCount}"/>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label for="updateIntroduction_${singer.singerId}" class="control-label">Introduction：</label>
                                                                            <textarea class="form-control" id="updateIntroduction_${singer.singerId}" name="updateIntroduction_${singer.singerId}" >${singer.introduction}</textarea>
                                                                        </div>

                                                                    </div>

                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                    <button type="submit" class="btn btn-primary" id="updateBtn_${singer.singerId}" >Confirm Edit</button>
                                                                </div>
                                                                </form>
                                                            </div>
                                                        </div>

                                                    </div>

                                                    <a class="btn btn-xs btn-default submenuitem" href="javascript:deleteSong(${singer.singerId})" target= "_self" title="Delete" data-toggle="tooltip"><i class="ftsucai-del-2"></i></a>

                                                </div>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                        </div>

                        <nav>
                            <ul class="pagination  no-border">
                                <c:if test="${sessionScope.singerPb.totalCount != 0}">

                                    <c:if test="${sessionScope.singerPb.currentPage == 1}">
                                        <li class="disabled">
                                            <a class="btn submenuitem" href="javascript:void(0);">
                                                <span><i class="ftsucai-146"></i></span>
                                            </a>
                                        </li>
                                    </c:if>
                                    <c:if test="${sessionScope.singerPb.currentPage != 1}">
                                        <li>
                                            <a class="btn submenuitem" href="${pageContext.request.contextPath}/admin/findSingerByPageServlet?currentPage=${sessionScope.singerPb.currentPage - 1}&rows=6&name=${sessionScope.singerCondition.name[0]}&sex=${sessionScope.singerCondition.sex[0]}&location=${sessionScope.singerCondition.location[0]}">
                                                <span><i class="ftsucai-146"></i></span>
                                            </a>
                                        </li>
                                    </c:if>


                                    <c:forEach begin="${sessionScope.singerPb.startPage}" end="${sessionScope.singerPb.endPage}" var="i" >
                                        <c:if test="${sessionScope.singerPb.currentPage == i}">
                                            <li class="active"><a  class="btn submenuitem" id="currentPage"
                                                                   href="${pageContext.request.contextPath}/admin/findSingerByPageServlet?currentPage=${i}&rows=6&name=${sessionScope.singerCondition.name[0]}&sex=${sessionScope.singerCondition.sex[0]}&location=${sessionScope.singerCondition.location[0]}">${i}</a></li>
                                        </c:if>
                                        <c:if test="${sessionScope.singerPb.currentPage != i}">
                                            <li><a  class="submenuitem" href="${pageContext.request.contextPath}/admin/findSingerByPageServlet?currentPage=${i}&rows=6&name=${sessionScope.singerCondition.name[0]}&sex=${sessionScope.singerCondition.sex[0]}&location=${sessionScope.singerCondition.location[0]}">${i}</a></li>
                                        </c:if>
                                    </c:forEach>

                                    <c:if test="${sessionScope.singerPb.currentPage == sessionScope.singerPb.totalPage}">
                                        <li class="disabled">
                                            <a class="btn submenuitem"
                                               href="javascript:void(0);">
                                                <span><i class="ftsucai-139"></i></span>
                                            </a>
                                        </li>
                                    </c:if>
                                    <c:if test="${sessionScope.singerPb.currentPage != sessionScope.singerPb.totalPage}">
                                        <li>
                                            <a class="btn submenuitem"
                                               href="${pageContext.request.contextPath}/admin/findSingerByPageServlet?currentPage=${sessionScope.singerPb.currentPage + 1}&rows=6&name=${sessionScope.singerCondition.name[0]}&sex=${sessionScope.singerCondition.sex[0]}&location=${sessionScope.singerCondition.location[0]}">
                                                <span><i class="ftsucai-139"></i></span>
                                            </a>
                                        </li>
                                    </c:if>

                                    <span style="font-size: 25px;margin-left: 5px;">
                                    Total ${sessionScope.singerPb.totalCount} Records/Total ${sessionScope.singerPb.totalPage} Pages
                                </span>
                                </c:if>
                                <c:if test="${sessionScope.singerPb.totalCount == 0}">
                                <span style="font-size: 25px;margin-left: 5px;">
                                    Have no singers now, please add some!
                                </span>
                                </c:if>
                            </ul>

                        </nav>
                    </div>
                </div>
            </div>

        </div>

    </div>

</main>
<!--End  Main page content-->
<script type="text/javascript" src="${pageContext.request.contextPath }/js/manager/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath }/js/manager/bootstrap.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath }/js/manager/perfect-scrollbar.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath }/js/manager/main.min.js"></script>
<script src="${pageContext.request.contextPath }/js/manager/bootstrap-notify.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath }/js/manager/tips.js"></script>
<%-- Tag insertion --%>
<script src="${pageContext.request.contextPath }/js/manager/jquery-tags-input/jquery.tagsinput.min.js"></script>
<script src="${pageContext.request.contextPath }/js/jquerysession.js"></script>
<script type="text/javascript">
    $(function(){
        $('.search-bar .dropdown-menu a').click(function() {
            var field = $(this).data('field') || '';
            $('#search-field').val(field);
            $('#search-btn').html($(this).text() + ' <span class="caret"></span>');
            $("#keyName").attr("placeholder", "Input" + $(this).text());
        });

        $('#keyName').blur(function (){
            var keyValue = $("#keyName").val();
            if(keyValue == null || keyValue == ""){
                return false;
            }else {
                window.location.href = "${pageContext.request.contextPath}/admin/findSingerByPageServlet?rows=6&" + $("#search-field").val() + "=" + keyValue;
            }
        });

    });

    function showMsg(msg){
        // alert(msg);
        if(msg == "" || msg == null){

        }else {
            tips.loading("show");
            tips.notify(msg, 'danger', 1000);
        }
    }

    function updateSinger(id){
        var accessCount = $("#update_accessCount_" + id).val();
        var reg = /^\d+$/;
        if(!reg.test(accessCount)){
            alert("Should be Non-negative integer~");
            return false;
        }
        var count = Number.parseInt(accessCount);
        if(count > 30000 || count < 0){
            alert("Initial heat should between 0~5000~");
            return false;
        }
        if(confirm("Are you sure to update？")) {
            return true;
        }else {
            return false;
        }
    }

    function deleteSong(id){
        //User safety tips
        if(confirm("Are you sure to delete？")){
            //Access path
            location.href="${pageContext.request.contextPath}/admin/deleteSingerServlet?singerId="+id;
        }
    }

    // Add an event to the delete button
    function delSelected(){
        if(confirm("Are you sure to delete this？")){
            let flag = false;
            // Judge if its selected
            let checks = document.getElementsByName("singerId");
            let str = "";
            for(let i = 0; i < checks.length; i++){
                if(checks[i].checked){
                    flag = true;
                    str += "singerId=" + checks[i].value + "&";
                    // break;
                }
            }

            if(flag) {
                window.location.href = "${pageContext.request.contextPath}/admin/deleteSingerServlet?" + str;
                // $("#form_del").submit();
            }else {
                return false;
            }
        }
    }

</script>
</body>
</html>

